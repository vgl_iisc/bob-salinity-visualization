## Description

This repository contains code and scripts for computing and tracking features that help visualize salinity propagation in the ocean.
The high salinity core is computed as an isovolume, it is further processed to identify fronts and skeletons, which are in turn tracked over time.
Advection points are computed within a region of high salinity water and tracked to help in the study of the role of ocean currents in salinity movement. 

---

## Requirements

No compilation required.
To run, you need to install python3 and following libraries in python3:
numpy, netCDF4, os, vtk, time, math, scipy, multiprocessing, matplotlib, datetime, networkx, functools, utils, pickle, sys, cloudvolume, concurrent, re, itertools, community, pandas, xarray.

Paraview(recommended version 5.9.0) is required for the visualization using paraview state files contained in each directory. State files created and tested with version 5.9.0. Paraview also need to have working paraview python as we use programmable filter for user interactive visualization.

User can download, extract and use the latest binaries for Paraview from https://www.paraview.org/download/

Input file used for experiments can be found at https://drive.google.com/file/d/1DUCc0MtPVhIIL8StVqDHuMu39IBGtJMC/view?usp=sharing

---

## Build

No build is required. Just copy the code and use as mentioned in each directory.

front_based: This folder contains all the files for front based tracking and a detailed readme for how to use it.

skeleton-based-salinity-tracking: This folder contains all the file for skeleton based tracking and a detailed readme for how to use it.

advection_based: This folder contains scripts and related files for advection based tracking and a readme. 

---

## Acknowledgements

This work is partially supported by an IoE grant from IISc Bangalore; a scholarship from MoE, Govt. of India; the BoBBLE (Bay of Bengal Boundary Layer Experiment) project funded by the Ministry of Earth Sciences, Govt. of India; a J. C. Bose Fellowship awarded by the SERB, DST, Govt. of India; a Swarnajayanti Fellowship from the Department of Science and Technology, India (DST/SJF/ETA-02/2015-16); a grant from SERB, Govt. of India (CRG/2021/005278); a Mindtree Chair research grant; National Supercomputing Mission, DST.

All datasets used for testing and validation are available from [https://www.nemo-ocean.eu/](https://www.nemo-ocean.eu/)

---

## Copyright

Copyright (c) 2021 Visualization & Graphics Lab (VGL), Indian Institute of Science. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Authors : Upkar Singh, Dhipu T M

Contact : upkarsingh [AT] iisc.ac.in, dhipuganesh [AT] gmail.com