import numpy as np
import os

fin = open('parameter.txt', 'r')
Lines = fin.readlines()

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())

CODE_PATH = '/home/sus/git_code_salinity/bob-salinity-visualization/front_based/'
#Give absolute path of the code here, starting and ending with "/"

pdi = self.GetInput()
pdo = self.GetOutput()

N = pdi.GetNumberOfPoints()

pt_lst=[]
for i in range(N):
	coord = pdi.GetPoint(i)
	pt_lst.append(coord)

np.save(CODE_PATH+'pt_lst.npy', pt_lst)

cmd = 'python3 '+CODE_PATH+'dfs_paths.py '+CODE_PATH
os.system(cmd)
'''
from paraview.simple import Show, Render , OpenDataFile, Tube, Calculator

canex2=OpenDataFile(CODE_PATH+'paths.vtp')

Show(canex2)

Render()
'''
print('done')