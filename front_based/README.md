## parameter.txt:
	
	Contains parameter values required by other scrpts. Change if needed, otherwise the default values are already set.

---

## TrackGraph.py:
	
	Computes the track graph and output track graph can be visualised using the paraview state file named TG_VIS.pvsm.

	To Execute: run "python3 TrackGraph.py"

---

## LongPath.py:
	
	Computes long paths clustered with source and destination surface front. Clustering matrix is a parameter in parameter.txt. Output can be visualised my using the paraview state file named path_box.pvsm

	To Execute: run "python3 LongPath.py"

---

## SelectPaths.py:
	
	This script allows user to interact with the TrackGraph. It is used along with the paraview state file named final_select.pvsm. We will explain usage with the usage of final_select.pvsm as it does not have ant usage on its own.

---

## dfs_paths.py:
	
	This file is called by SelectPath.py. Please dont delete or move from directorty. User need not interact with this file.

---

## TG_VIS.pvsm:
	
	Usage:
		1. Open paraview
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.

---

## path_box.pvsm:

	Usage:
		1. Open paraview
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.

---

## final_select.pvsm:
	
	WE NEED TO EXECUTE TrackGraph.py BEFORE WE USE THIS STATE FILE AS IT NEEDS THE TRACK GRAPH AS INPUT.
	
	Usage:
		1. Open paraview.
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.
		6. Select a set of points using "Select points with a polygon" option in paraview GUI.
		7. Select ExtractSelection1 filter and press "Copy active selection" button in its properties and press "Apply".
		8. Open SelectPaths.py and look for variable CODEPATH.
		9. Give the absolute path of code directory i.e. path of front_based code directory in this variable.
		10. Replace the code in ProgrammableFilter1 with code from SelectPaths.py.
		11. Apply and render the ProgrammableFilter1
		12. Right click paths.vtp and click on Reload Files option.		

	Alternatively:
		1. Open paraview.
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.
		6. Select a set of points using "Select points with a polygon" option in paraview GUI.
		7. Select ExtractSelection1 filter and press "Copy active selection" button in its properties and press "Apply".
		8. Open the code of ProgrammableFilter1 and look for variable CODEPATH.
		9. Give the absolute path of code directory i.e. path of front_based code directory in this variable.
		10. Apply and render the ProgrammableFilter1
		11. Right click paths.vtp and click on Reload Files option.		