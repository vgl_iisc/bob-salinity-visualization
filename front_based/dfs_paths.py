#new_track_mult
import numpy as np
import netCDF4
import os
import sys
import vtk
from vtk import vtkPolyDataWriter, vtkMutableDirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter
import time
import math
from scipy import ndimage
import multiprocessing as mp
import matplotlib.pyplot as plt
import datetime
import networkx as nx

fin = open('parameter.txt', 'r')
Lines = fin.readlines()

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())
		#print(parameters)

lt_origin = int(parameters[1])
ln_origin = int(parameters[2])

d_max = int(parameters[7])

resolution = float(parameters[8].split('/')[0].strip())/float(parameters[8].split('/')[1].strip())

def distance(p1, p2):
	x_d = p2[0]-p1[0]
	y_d = p2[1]-p1[1]
	#return math.sqrt((x_d*x_d)+((y_d*y_d)**5))
	#return abs(y_d)
	return y_d

CODE_PATH = str(sys.argv[1])
#Give absolute path of the code here, starting and ending with "/"

d_lst = np.load(CODE_PATH+'d_lst.npy').astype('int')
cluster_rep = np.load('cluster_rep_clipped.npy', allow_pickle=True).item()

shape = [d_max, 0, 0, 0]

for d in range(shape[0]):
	k_vd = np.load('k_v_clipped/k_v'+str(d)+'.npy', allow_pickle=True).item()
	for (_, t, lt, ln) in k_vd:
		if t+1>shape[1]:
			shape[1] = t+1
		if lt+1>shape[2]:
			shape[2] = lt+1
		if ln+1>shape[3]:
			shape[3] = ln+1

print(shape)

max_ct = 0
for d in range(shape[0]):
	edges_d = np.load('edg_clipped/edges'+str(d)+'.npy', allow_pickle=True).item()
	for e, ct in edges_d.items():
		if max_ct<ct:
			max_ct=ct
	del edges_d

print(max_ct, 'max_ct')

points = np.load(CODE_PATH+'pt_lst.npy')

graph = nx.DiGraph()

for d in range(shape[0]):
	edges_d = np.load('edg_clipped/edges'+str(d)+'.npy', allow_pickle=True).item()
	for e, ct in edges_d.items():
		s1 = int(e[0])
		s2 = int(e[1])
		if not cluster_rep.get(s1):
			continue
		if not cluster_rep.get(s2):
			continue
		[t1, d1, y1, x1] = cluster_rep[s1]
		[t2, d2, y2, x2] = cluster_rep[s2]
		graph.add_edge(s1, s2, weight=(max_ct-ct))#, weight=wt_fact-distance([x1,y1,d1], [x2,y2,d2]) )
	del edges_d

in_d = graph.in_degree()
out_d = graph.out_degree()

src_lst = []
for i in in_d:
	if i[1]==0:
		src_lst.append(i[0])

dst_lst=[]
for i in out_d:
	if i[1]==0:
		dst_lst.append(i[0])

print('paths')

start = time.time()

new_points = {}

map_c = {}
for key, val in cluster_rep.items():
	val1 = []
	for i in val:
		val1.append(float(i))
	val1[3] = ln_origin+(val1[3]*resolution)
	val1[2] = lt_origin+(val1[2]*resolution)
	for i in range(4): val1[i] = round(val1[i],4)
	map_c[tuple(val1)] = key

for pt in points:
	d = round(abs(pt[2]),4)
	lt = round(pt[1],4)
	ln = round(pt[0],4)
	for t in range(shape[1]):
		if map_c.get((t, d, lt, ln)):
			if graph.has_node(map_c[(t, d, lt, ln)]):
				new_points[map_c[(t, d, lt, ln)]]=1

new_points = list(new_points.keys())

total_pts = len(new_points)
parts = (os.cpu_count())
step = int(total_pts/parts)+1

def foo(i):
	path_i = []
	for pi in range(i*step, (i+1)*step):
		path_pi = []
		if not(pi<total_pts):
			break
		
		pt = new_points[pi]
		
		far_dst = pt
		pts_dst = nx.single_source_dijkstra_path(graph, pt)
		keys = pts_dst.keys()
		for dst in dst_lst:
			if dst in keys:
				
				[_,_,yd, xd] = cluster_rep[dst]
				[_,_,yf, xf] = cluster_rep[far_dst]
				[_,_,yp, xp] = cluster_rep[pt]
				
				#distance matric
				dist_p_d = distance([xp,yp], [xd,yd])
				dist_p_f = distance([xp,yp], [xf,yf])

				if dist_p_d > dist_p_f:
					far_dst = dst

		lp = pts_dst[far_dst]
		
		path_pi.append(lp)
		
		far_dst = pt
		pts_dst = nx.single_source_dijkstra_path(graph.reverse(), pt)
		keys = pts_dst.keys()
		for dst in src_lst:
			if dst in keys:
				
				[_,_,yd, xd] = cluster_rep[dst]
				[_,_,yf, xf] = cluster_rep[far_dst]
				[_,_,yp, xp] = cluster_rep[pt]
				
				#distance matric
				dist_p_d = distance([xp,yp], [xd,yd])
				dist_p_f = distance([xp,yp], [xf,yf])

				if dist_p_d < dist_p_f:
					far_dst = dst

		lp = pts_dst[far_dst]
		path_pi.append(lp)
		path_i.append(path_pi)
		del path_pi

	return path_i
		
pool = mp.Pool()
paths = pool.map(foo, np.arange(parts))
pool.close()
pool.join()
del pool


graph_vtk = vtkMutableDirectedGraph()
points = vtkPoints()
weights = vtk.vtkDoubleArray()
weights.SetNumberOfComponents(1)
weights.SetName('Point')

weights1 = vtk.vtkDoubleArray()
weights1.SetNumberOfComponents(1)
weights1.SetName('Time')


i_d = 0
for path_i in paths:
	for path_pi in path_i:

		lp = path_pi[0]
		size = len(lp)
		
		#print(i_d, size)
		
		for n in range(size-1):
			s1 = lp[n]
			s2 = lp[n+1]

			[t1, d1, y1, x1] = cluster_rep[s1]
			[t2, d2, y2, x2] = cluster_rep[s2]
		
			#print(s1,cluster_rep[s1], s2,cluster_rep[s2])
			#input()

			x1 = ln_origin+(x1*resolution)
			y1 = lt_origin+(y1*resolution)
			x2 = ln_origin+(x2*resolution)
			y2 = lt_origin+(y2*resolution)
			
			vertex1 = graph_vtk.AddVertex()
			points.InsertNextPoint([x1, y1, -d1])
			weights1.InsertNextValue(t1)
			vertex2 = graph_vtk.AddVertex()
			points.InsertNextPoint([x2, y2, -d2])
			weights1.InsertNextValue(t2)
			graph_vtk.AddEdge (vertex1, vertex2)
			weights.InsertNextValue(i_d)
		
		lp = path_pi[1]
		size = len(lp)

		for n in range(size-1):
			s1 = lp[n]
			s2 = lp[n+1]

			[t1, d1, y1, x1] = cluster_rep[s1]
			[t2, d2, y2, x2] = cluster_rep[s2]
			
			x1 = ln_origin+(x1*resolution)
			y1 = lt_origin+(y1*resolution)
			x2 = ln_origin+(x2*resolution)
			y2 = lt_origin+(y2*resolution)
			
			vertex1 = graph_vtk.AddVertex()
			points.InsertNextPoint([x1, y1, -d1])
			weights1.InsertNextValue(t1)
			vertex2 = graph_vtk.AddVertex()
			points.InsertNextPoint([x2, y2, -d2])
			weights1.InsertNextValue(t2)
			graph_vtk.AddEdge (vertex1, vertex2)
			weights.InsertNextValue(i_d)
		
		i_d+=1

graph_vtk.SetPoints(points)
graph_vtk.GetEdgeData().AddArray(weights);
graph_vtk.GetVertexData().AddArray(weights1);

graphToPolyData = vtkGraphToPolyData()
graphToPolyData.SetInputData(graph_vtk)
graphToPolyData.Update()

writer = vtkXMLPolyDataWriter()
writer.SetInputData(graphToPolyData.GetOutput())

writer.SetFileName(CODE_PATH+'paths.vtp')

writer.Write()
del graph_vtk
del points

print('paths: points=',total_pts,', time=' ,time.time()-start)

