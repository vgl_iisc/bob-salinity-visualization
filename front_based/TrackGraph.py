import numpy as np
import netCDF4
import os
import vtk
from vtk import vtkPolyDataWriter, vtkMutableDirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter
import time
import math
from scipy import ndimage
import multiprocessing as mp
import matplotlib.pyplot as plt
import datetime
import networkx as nx

fin = open('parameter.txt', 'r')
Lines = fin.readlines()

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())
		#print(parameters)

infile = str(parameters[5])

filename = str(parameters[6])

v_limit = int(parameters[0])

lt_origin = int(parameters[1])
ln_origin = int(parameters[2])

lt_clip = int(parameters[3])
ln_clip = int(parameters[4])

interpolation = int(parameters[10])

d_max = int(parameters[7])

resolution = float(parameters[8].split('/')[0].strip())/float(parameters[8].split('/')[1].strip())

neighborhood = int(parameters[9])

v_name = str(parameters[11])

d_name = str(parameters[12])

nbrhd = int(neighborhood/(resolution*111))

print(datetime.datetime.now())

def euc_distance(p1,p2):
	dist = 0
	for i in range(len(p1)):
		diff = p1[i]-p2[i]
		dist += (diff**2)
	return math.sqrt(dist)

start = time.time()

cmd = "rm data.nc"
os.system(cmd)
cmd = "cp "+infile+" data.nc"
os.system(cmd)

DS = netCDF4.Dataset("data.nc", "a")

shape = DS.variables[v_name].shape

d_lst = np.copy(DS.variables[d_name][:]).astype('int')

np.save('d_lst.npy', d_lst)

print(d_max,' d_max')

print(shape)

fn = np.zeros((shape[0], shape[1], shape[2], shape[3]))
for t in range(shape[0]):
	fn[t] = np.copy(DS.variables[v_name][t].astype('float64')) 

def foo3(d):
	k_vd = np.zeros((shape[0], shape[2], shape[3]))
	d_c = d
	while(d_c < d_lst[-1]):
		di = np.where(d_lst == d_c)[0]
		if(len(di)==1):
			break
		d_c+=1
	di = di[0]
	
	for t in range(shape[0]):
		if d == d_lst[di]:
			k_vd[t] = np.copy(fn[t][di])
		else:
			v_mx = fn[t][di]
			v_mn = fn[t][di-1]
			alpha = (d_lst[di]-d) / (d_lst[di]-d_lst[di-1])
			val = ((alpha) * v_mn + (1-alpha) * v_mx)
			k_vd[t] = np.copy(val)
	k_vd[k_vd<v_limit]=0
	k_vd[k_vd>0]=1
	np.save('fn/fn'+str(d)+'.npy', k_vd)
	del k_vd	

print('interpolating')

cmd = "rm -r fn"
os.system(cmd)
cmd = 'mkdir fn'
os.system(cmd)

pool = mp.Pool()
pool.map(foo3, np.arange(d_max))
pool.close()
pool.join()
del pool

print('interpolating done')
del fn
del DS

def foo1(d): #computing boundary
	k_vd = {}
	ct = 1
	fn = np.load('fn/fn'+str(d)+'.npy', allow_pickle=True)
	for t in range(shape[0]):
		fn_td = np.copy(fn[t])
		avg = ndimage.uniform_filter(fn_td, size=3)
		avg = avg+1
		avg[avg<2] = 1
		avg[avg>1] = 0
		bound = np.multiply(fn_td,avg)
		coord_lst = np.where(bound == 1)
		boundary_graph = nx.Graph()
		for ind in range(len(coord_lst[0])):
			lt = coord_lst[0][ind]
			ln = coord_lst[1][ind]
			for lt1 in range(lt-1, lt+2):
				if lt1<0 or lt1>shape[2]-1:
					continue
				for ln1 in range(ln-1, ln+2):
					if ln1<0 or ln1>shape[3]-1:
						continue
					if (bound[lt1][ln1])==1:
						boundary_graph.add_edge((lt,ln),(lt1, ln1), weight=(2*shape[2])-(lt+lt1))
		
		comps = nx.connected_components(boundary_graph)
		new_comps = [s for s in comps]
		del comps
		east = []
		west = []
		for comp in new_comps:
			(lte,lne) = (0,0)
			(ltw,lnw) = (0,shape[3])
			for (lt,ln) in comp:
				if ln>=lne:
					(lte,lne) = (lt,ln)
				if ln<=lnw:
					(ltw,lnw) = (lt,ln)
			east.append((lte,lne))
			west.append((ltw,lnw))

		for ind in range(len(east)):
			dst = east[ind]
			src = west[ind]
			(lt, ln) = src
			(ltr, lnr) = (lt-1, ln-1)
			stop = (ltr, lnr)
			path = []
			path.append(src)
			nbrs = list(boundary_graph.neighbors((lt,ln)))
			while((ltr,lnr)!=dst):

				if ltr>lt and lnr<=ln:
					lnr+=1
				elif lnr>ln and ltr>=lt:
					ltr-=1
				elif ltr<lt and lnr>=ln:
					lnr-=1
				elif lnr<ln and ltr<=lt:
					ltr+=1
				
				if (ltr, lnr) in nbrs:
					path.append((ltr,lnr))
					temp = (lt,ln)
					(lt,ln) = (ltr,lnr)
					nbrs = list(boundary_graph.neighbors((lt,ln)))
					(ltr,lnr) = temp
					stop = (ltr, lnr)
					continue
					
				if (ltr, lnr) == stop:
					break
					
			for (lt,ln) in path:
				k_vd[(d,t,lt-1,ln-1)] = ct
				ct+=1
	
	np.save('k_v_clipped/k_v'+str(d)+'.npy', k_vd)
	del k_vd
	
print('boundary')

cmd = "rm -r k_v_clipped"
os.system(cmd)
cmd = "mkdir k_v_clipped"
os.system(cmd)

pool = mp.Pool()
pool.map(foo1, np.arange(d_max))
pool.close()
pool.join()
del pool

print('boundary done')

def foo(d): #clustering
	edges = dict()
	k_vd = np.load('k_v_clipped/k_v'+str(d)+'.npy', allow_pickle=True).item()
	flag = False
	if d+1<d_max:
		flag = True
		k_vd1 = np.load('k_v_clipped/k_v'+str(d+1)+'.npy', allow_pickle=True).item()
	for key, ind in k_vd.items():
		(d_, t, lt, ln) = key
		for lt1 in range(lt-nbrhd, lt+nbrhd+1):
			if lt1<0 or lt1>shape[2]-1:
				continue
			for ln1 in range(ln-nbrhd, ln+nbrhd+1):
				if ln1<0 or ln1>shape[3]-1:
					continue
				if (((lt-lt1)**2)+((ln-ln1)**2)>nbrhd**2):
					continue
				
				if k_vd.get((d,t,lt1,ln1)):
					e = (tuple((d, t, lt, ln)), tuple((d, t, lt1, ln1)))
					if not(edges.get(e)):
						edges[e]=1
				
				if flag and k_vd1.get((d+1,t,lt1,ln1)):
					e = (tuple((d, t, lt, ln)), tuple((d+1, t, lt1, ln1)))
					if not(edges.get(e)):
						edges[e]=1

	#np.save('edg/edges'+str(d)+'.npy', edges)
	#del edges
	return list(edges.keys())

print('clustering')

#cmd = "rm -r edg_clipped"
#os.system(cmd)
#cmd = "mkdir edg_clipped"
#os.system(cmd)

st = time.time()
pool = mp.Pool()
edges_d = pool.map(foo, np.arange(d_max))
pool.close()
pool.join()
del pool

clustering_graph = nx.Graph()

print('graph')

for d in range(d_max):
	k_vd = np.load('k_v_clipped/k_v'+str(d)+'.npy', allow_pickle=True).item()
	nodes = list(k_vd.keys())
	clustering_graph.add_nodes_from(nodes)
	#del k_vd, nodes

print('nodes = ', clustering_graph.number_of_nodes())

edges = [ed for l in edges_d for ed in l]
print(len(edges))
del edges_d

clustering_graph.add_edges_from(edges)
del edges
print('edges = ', clustering_graph.number_of_edges())

comp = nx.connected_components(clustering_graph)
final_clusters = [list(s) for s in comp]
del comp, clustering_graph

k_v = []
for d in range(d_max):
	k_vd = dict()
	k_v.append(k_vd)

print('connected_components')

for cluster in range(1, len(final_clusters)+1):
	new_cluster = final_clusters[cluster-1]
	for (d, t, lt, ln) in new_cluster:
		k_v[d][(d, t, lt, ln)] = cluster

for d in range(d_max):
	np.save('k_v_clipped/k_v'+str(d)+'.npy', k_v[d])

del k_v

print('clustering done', len(final_clusters))

def foo2(d): #edges
	edges = dict()
	k_vd = np.load('k_v_clipped/k_v'+str(d)+'.npy', allow_pickle=True).item()
	flag = False
	if d+1<d_max:
		flag = True
		k_vd1 = np.load('k_v_clipped/k_v'+str(d+1)+'.npy', allow_pickle=True).item()
	for key, cluster in k_vd.items():
		(_, t, lt, ln) = key
		for lt1 in range(lt-nbrhd, lt+nbrhd+1):
			if lt1<0 or lt1>shape[2]-1:
				continue
			for ln1 in range(ln-nbrhd, ln+nbrhd+1):
				if ln1<0 or ln1>shape[3]-1:
					continue
				if (((lt-lt1)**2)+((ln-ln1)**2)>nbrhd**2):
					continue

				if t+1>=shape[0]:
					continue
				if k_vd.get((d,t+1,lt1,ln1)):
					e = (k_vd[(d,t,lt,ln)], k_vd[(d,t+1,lt1,ln1)])
					if edges.get(e):
						edges[e] += 1
					else:
						edges[e] = 1
			
				if flag and k_vd1.get((d+1,t+1,lt1,ln1)):
					e = ( k_vd[(d,t,lt,ln)], k_vd1[(d+1,t+1,lt1,ln1)] )
					if edges.get(e):
						edges[e] += 1
					else:
						edges[e] = 1

	np.save('edg_clipped/edges'+str(d)+'.npy', edges)
	del edges
	#return edges
print('edges')

cmd = "rm -r edg_clipped"
os.system(cmd)
cmd = "mkdir edg_clipped"
os.system(cmd)

pool = mp.Pool()
#pair_lst= 
pool.map(foo2, np.arange(d_max))
pool.close()
pool.join()
del pool

cluster_rep = dict()
o_ct=0
for cluster in range(1, len(final_clusters)+1):
	ln_lim = (ln_clip-ln_origin)/resolution
	lt_lim = (lt_clip-lt_origin)/resolution
	d_sum = 0
	lt_sum = 0
	ln_sum = 0
	ct = 0
	min_dist = float('inf')
	for (d, t, lt, ln) in final_clusters[cluster-1]:
		if lt<=lt_lim:
			#lt = lt_lim
			continue
		if ln<=ln_lim:
			#ln = ln_lim
			continue
		d_sum += d
		lt_sum += lt
		ln_sum += ln
		ct += 1

	if ct!=0:
		mean_pt = [d_sum/ct, lt_sum/ct, ln_sum/ct]
		for (d, t, lt, ln) in final_clusters[cluster-1]:
			if lt<=lt_lim:
				continue
			if ln<=ln_lim:
				continue
			if min_dist>euc_distance([d, lt, ln], mean_pt):
				min_dist = euc_distance([d, lt, ln], mean_pt)
				cluster_rep[cluster] = [t, d, lt, ln]
	else:
		o_ct += 1

np.save('cluster_rep_clipped.npy', cluster_rep)
np.save('final_clusters_clipped.npy', final_clusters)

print('edges done\n********************\ntotal time = ',(time.time()-start)/60)

graph_vtk = vtkMutableDirectedGraph()
points = vtkPoints()
time = vtk.vtkDoubleArray()
time.SetNumberOfComponents(1)
time.SetName('Time')
cluster_size = vtk.vtkDoubleArray()
cluster_size.SetNumberOfComponents(1)
cluster_size.SetName('Cluster Size')
cluster_number = vtk.vtkDoubleArray()
cluster_number.SetNumberOfComponents(1)
cluster_number.SetName('Cluster Number')
edge_size = vtk.vtkDoubleArray()
edge_size.SetNumberOfComponents(1)
edge_size.SetName('Edges')

vertex_list = []
vertex_list.append('NULL')
for cluster in range(1, len(final_clusters)+1): #insert vertices
	if not cluster_rep.get(cluster):
		vertex_list.append('NULL')
		continue
	[t1, d1, y1, x1] = cluster_rep[cluster]
	vertex = graph_vtk.AddVertex()
	x1 = ln_origin+(x1*resolution)
	y1 = lt_origin+(y1*resolution)
	points.InsertNextPoint([x1, y1, -d1])
	time.InsertNextValue(t1)
	cluster_size.InsertNextValue(len(final_clusters[cluster-1]))
	cluster_number.InsertNextValue(cluster)
	vertex_list.append(vertex)

del final_clusters, cluster_rep

for d in range(d_max): #insert edges
	edges_d = np.load('edg_clipped/edges'+str(d)+'.npy', allow_pickle=True).item()
	for e, ct in edges_d.items():
		s1 = int(e[0])
		s2 = int(e[1])
		if vertex_list[s1]=='NULL' or vertex_list[s2]=='NULL':
			continue
		graph_vtk.AddEdge (vertex_list[s1], vertex_list[s2])
		edge_size.InsertNextValue(ct)
	del edges_d
		
del vertex_list

graph_vtk.SetPoints(points)
graph_vtk.GetVertexData().AddArray(time)
graph_vtk.GetVertexData().AddArray(cluster_size)
graph_vtk.GetVertexData().AddArray(cluster_number)
graph_vtk.GetEdgeData().AddArray(edge_size)

graphToPolyData = vtkGraphToPolyData()
graphToPolyData.SetInputData(graph_vtk)
graphToPolyData.Update()

writer = vtkXMLPolyDataWriter()
writer.SetInputData(graphToPolyData.GetOutput())

writer.SetFileName(filename+".vtp")

writer.Write()
del graph_vtk
del points