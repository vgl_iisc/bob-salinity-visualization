import os
import re
import sys
import math
import networkx as nx
import itertools

from dataIO import *
from skeletonization import *
from utils import *

import community
import matplotlib.pylab as plt

import vtk, vtk.util.numpy_support
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from vtk import vtkPolyDataWriter, vtkMutableUndirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter

""" Calculates the Euclidean distance between two pixels """
def distance( a,b,c,d,e,f ):
	#print(a,b,c,d,e,f)
	return (int(a-d))**2 + (int(b-e))**2 + (int(c-f))**2


### Forward MAtching ###

skipinterval = 20

timeNodeId = 0
timeNodesDict = dict()
timeNodes = np.array([])
timeGraph = nx.Graph()

def getGlobalNodeId(node):
	global timeNodeId
	global timeNodes
	global timeNodesDict
	global timeGraph
	key = str(node[0])+str(node[1])+str(node[2])
	if key not in timeNodesDict:
		if timeNodeId == 0:
			timeNodes = [node]
		else:
			timeNodes = np.concatenate((timeNodes,[node]))
		timeNodesDict[key] = timeNodeId
		timeNodeId = timeNodeId + 1
	
	return timeNodesDict[key]
	
def getGlobalNode(nodeId):
	global timeNodes
	return timeNodes[nodeId]
	
for timestep in range(121):
	print("Forward Timestep: "+str(timestep))
	cfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep)+".pkl"
	nfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep+1)+".pkl"
	
	cskel = load_skeleton(cfile)
	nskel = load_skeleton(nfile)
	
	cskel['nodes'] = cskel['nodes'].astype(np.int64)
	nskel['nodes'] = nskel['nodes'].astype(np.int64)
	
	#print(cskel)
	
	tskel = Skeleton()
	nodes = np.array([])
	edges = np.array([])
	first = 0	
	
	cskelGraph = nx.Graph()
	nskelGraph = nx.Graph()

	for c in range(cskel['edges'].shape[0]):
		if(cskel['nodes'][cskel['edges'][c][0]][2] > 50 and cskel['nodes'][cskel['edges'][c][1]][2] > 50): # Filter the lower depth values
			cskelGraph.add_edge(cskel['edges'][c][0], cskel['edges'][c][1])
		
	for n in range(nskel['edges'].shape[0]):
		if(nskel['nodes'][nskel['edges'][n][0]][2] > 50 and nskel['nodes'][nskel['edges'][n][1]][2] > 50):
			nskelGraph.add_edge(nskel['edges'][n][0], nskel['edges'][n][1])		
	
	
	cskelComponents = nx.connected_components(cskelGraph)
	
	
	comp = 1
	for cnodes in cskelComponents:
		neighbordist = 1000000
		cnode = cskel['nodes'][list(cnodes)[0]]
		neighbornode = nskel['nodes'][0]
		
		print("##### Processing Component: "+str(comp))
		cskip = 0
		clist = sorted(cnodes)
		for c in clist:
			if cskip%skipinterval == 0:
				neighbordist = 1000000		
				node1 = cskel['nodes'][c]	
				nskelComponents = nx.connected_components(nskelGraph)
				for nnodes in nskelComponents:	
					nskip = 0
					nlist = sorted(nnodes)			
					for n in nlist:	
						if nskip%skipinterval == 0:			
							node2 = nskel['nodes'][n]

							#if node2[0] >= node1[0] and node2[1] >= node1[1]: 					
							dx = (node1[0]-node2[0])**2
							dy = (node1[1]-node2[1])**2
							dz = (node1[2]-node2[2])**2
							
							dist = dx + dy + dz
							
							if dist < neighbordist:
								cnode = node1
								neighbornode = node2
								neighbordist = dist
								#print(cnode, neighbornode, neighbordist)	
										
						nskip = nskip + 1	
											
				if neighbordist < 1000000: #and neighbornode[0] >= cnode[0] and neighbornode[1] >= cnode[1]: #Stricly Forward
					print(cnode, neighbornode, neighbordist)
					if first == 0:
						nodes = [cnode]
						nodes = np.concatenate((nodes,[neighbornode]))
						edges = [[cnode], [neighbornode]]
						
						first = 1
					else:		
						nodes = np.concatenate((nodes,[cnode]))
						nodes = np.concatenate((nodes,[neighbornode]))
						edge = [[cnode], [neighbornode]]				
						edges = np.concatenate((edges,edge))
						#break
					
					id1 = getGlobalNodeId(cnode)
					id2 = getGlobalNodeId(neighbornode)
					timeGraph.add_edge(id1, id2, weight=neighbordist)
						
			cskip = cskip + 1
				
		comp = comp + 1						

	# Consolidate nodes and edges
	#tskel.nodes = np.unique(nodes, axis=0)
	#tskel.edges = np.unique(edges, axis=0)
	tskel.nodes = nodes
	tskel.edges = nodes
	
	#print(tskel.nodes)
	#print(tskel.edges)

	# Write to VTP File
	graph = vtkMutableUndirectedGraph()
	points = vtkPoints()	
	flag=0
	vertex1=[]
	vertex2=[]
	for node in tskel.nodes:
		if flag == 0:
			vertex1 = graph.AddVertex()
			points.InsertNextPoint(node)
			flag = 1
		else:	
			vertex2 = graph.AddVertex()
			graph.AddEdge (vertex1, vertex2)
			points.InsertNextPoint(node)
			flag = 0

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	outfile = "./tracks/new/35PSU_Track_F_"+"{:03d}".format(timestep)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
	
	outfile = "./tracks/new/35PSU_Track_F_"+"{:03d}".format(timestep)+".pkl"
	save_skeleton(tskel,outfile)		
	
	#break		

exit(0)

### Backward Matching ####	
graph = vtkMutableUndirectedGraph()
points = vtkPoints()
skipinterval = 50
	
for timestep in range(121):
	print("Backward Timestep: "+str(timestep+1))
	cfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep+1)+".pkl"
	nfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep)+".pkl"
	
	cskel = load_skeleton(cfile)
	nskel = load_skeleton(nfile)
	
	cskel['nodes'] = cskel['nodes'].astype(np.int64)
	nskel['nodes'] = nskel['nodes'].astype(np.int64)
	
	#print(cskel)
	
	tskel = Skeleton()
	nodes = np.array([])
	edges = np.array([])
	first = 0	
	
	cskelGraph = nx.Graph()
	nskelGraph = nx.Graph()

	for c in range(cskel['edges'].shape[0]):
		if(cskel['nodes'][cskel['edges'][c][0]][2] > 50 and cskel['nodes'][cskel['edges'][c][1]][2] > 50):
			cskelGraph.add_edge(cskel['edges'][c][0], cskel['edges'][c][1])
		
	for n in range(nskel['edges'].shape[0]):
		if(nskel['nodes'][nskel['edges'][n][0]][2] > 50 and nskel['nodes'][nskel['edges'][n][1]][2] > 50):
			nskelGraph.add_edge(nskel['edges'][n][0], nskel['edges'][n][1])		
	
	
	cskelComponents = nx.connected_components(cskelGraph)
	
	
	comp = 1
	for cnodes in cskelComponents:
		neighbordist = 1000000
		cnode = cskel['nodes'][list(cnodes)[0]]
		neighbornode = nskel['nodes'][0]
		
		print("##### Processing Component: "+str(comp))
		cskip = 0
		clist = sorted(cnodes)
		for c in clist:
			if cskip%skipinterval == 0:
				neighbordist = 1000000		
				node1 = cskel['nodes'][c]	
				nskelComponents = nx.connected_components(nskelGraph)
				for nnodes in nskelComponents:	
					nskip = 0
					nlist = sorted(nnodes)			
					for n in nlist:	
						if nskip%skipinterval == 0:			
							node2 = nskel['nodes'][n]
							
							#if node2[0] <= node1[0] and node2[1] <= node1[1]:	#Stricly Backward				
							dx = (node1[0]-node2[0])**2
							dy = (node1[1]-node2[1])**2
							dz = (node1[2]-node2[2])**2
							
							dist = dx + dy + dz
							
							if dist < neighbordist:
								cnode = node1
								neighbornode = node2
								neighbordist = dist
								#print(cnode, neighbornode, neighbordist)	
										
						nskip = nskip + 1	
							
				if neighbordist < 1000000:
					print(cnode, neighbornode, neighbordist)
					if first == 0:
						nodes = [cnode]
						nodes = np.concatenate((nodes,[neighbornode]))
						edges = [[cnode], [neighbornode]]
						
						first = 1
					else:		
						nodes = np.concatenate((nodes,[cnode]))
						nodes = np.concatenate((nodes,[neighbornode]))
						edge = [[cnode], [neighbornode]]				
						edges = np.concatenate((edges,edge))
						#break
						
					id1 = getGlobalNodeId(cnode)
					id2 = getGlobalNodeId(neighbornode)
					timeGraph.add_edge(id1, id2, weight=neighbordist)						
						
			cskip = cskip + 1
				
		comp = comp + 1						

	# Consolidate nodes and edges
	#tskel.nodes = np.unique(nodes, axis=0)
	#tskel.edges = np.unique(edges, axis=0)
	tskel.nodes = nodes
	tskel.edges = nodes
	
	#print(tskel.nodes)
	#print(tskel.edges)

	# Write to VTP File
	flag=0
	vertex1=[]
	vertex2=[]
	for node in tskel.nodes:
		if flag == 0:
			vertex1 = graph.AddVertex()
			points.InsertNextPoint(node)
			flag = 1
		else:	
			vertex2 = graph.AddVertex()
			graph.AddEdge (vertex1, vertex2)
			points.InsertNextPoint(node)
			flag = 0

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()

	outfile = "./tracks/backward-1/35PSU_Track_B_"+"{:03d}".format(timestep)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
		
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".pkl"
	save_skeleton(tskel,outfile)	
	
	#break					

'''
graph = vtkMutableUndirectedGraph()
points = vtkPoints()

timeMST = nx.tree.minimum_spanning_edges(timeGraph, algorithm='kruskal', data=False)
edgelist = list(timeMST)

# Write to VTP File
vertex1=[]
vertex2=[]
for edge in edgelist:
	vertex1 = graph.AddVertex()
	points.InsertNextPoint(getGlobalNode(edge[0]))	
	vertex2 = graph.AddVertex()
	points.InsertNextPoint(getGlobalNode(edge[1]))
	graph.AddEdge (vertex1, vertex2)

graph.SetPoints(points)

graphToPolyData = vtkGraphToPolyData()
graphToPolyData.SetInputData(graph)
graphToPolyData.Update()

writer = vtkXMLPolyDataWriter()
writer.SetInputData(graphToPolyData.GetOutput())

outfile = "./tracks/35PSU_Time_Track.vtp"
writer.SetFileName(outfile)
writer.Write()
'''

'''
comp = nx.algorithms.community.girvan_newman(timeGraph)
k = 1
for communities in itertools.islice(comp,k):
	print("Iteration")
	print(tuple(sorted(c) for c in communities))
'''

dendrogram = community.generate_dendrogram(timeGraph)
for level in range(len(dendrogram) - 1) :
	print("partition at level", level, "is", community.partition_at_level(dendrogram, level))

#drawing
partition = community.best_partition(timeGraph)
pos = nx.spring_layout(timeGraph)
plt.figure(figsize=(8, 8))
plt.axis('off')
nx.draw_networkx_nodes(timeGraph, pos, node_size=600, cmap=plt.cm.RdYlBu, node_color=list(partition.values()))
nx.draw_networkx_edges(timeGraph, pos, alpha=0.3)
plt.show(timeGraph)
	
'''
print(comp)
print('k=0')
print(next(comp))
print('k=1')
print(next(comp))
'''




'''
print("Number of CC: "+str(nx.number_connected_components(timeGraph)))	
timeComponents = nx.connected_components(timeGraph)
for nodes in timeComponents:
	print(list(nodes))
'''
			
'''		
	for c in range(cskel['nodes'].shape[0]):
		cnode = cskel['nodes'][c]
		v1 = c
		v2 = c
		#print(cnode)
		neighbornode = nskel['nodes'][0]
		neighbordist = 1000000
		for n in range(nskel['nodes'].shape[0]):
			nnode = nskel['nodes'][n]
			if cnode[0] != nnode[0] and cnode[1] != nnode[1] and cnode[2] != nnode[2]:
				#print(cnode)
				#print(nnode)
				#print(cnode-nnode)
				#dist = distance(cnode[0],cnode[1],cnode[2],nnode[0],nnode[1],nnode[2])
				dx = (cnode[0]-nnode[0])**2
				dy = (cnode[1]-nnode[1])**2
				dz = (cnode[2]-nnode[2])**2
				#print(dx, dy, dz)
				dist = dx + dy + dz
				#print(int(cnode[0]),int(cnode[1]),int(cnode[2]),int(nnode[0]),int(nnode[1]),int(nnode[2]), dist, neighbordist)
				if dist < neighbordist:
					neighbordist = dist
					neighbornode = nnode
					v2 = n
		
		if first == 0:
			nodes = [cnode]
			nodes = np.concatenate((nodes,[neighbornode]))
			edges = [[cnode], [neighbornode]]
			
			first = 1
		else:		
			nodes = np.concatenate((nodes,[cnode]))
			nodes = np.concatenate((nodes,[neighbornode]))
			edge = [[cnode], [neighbornode]]				
			edges = np.concatenate((edges,edge))
			#break
		
	# Consolidate nodes and edges
	#tskel.nodes = np.unique(nodes, axis=0)
	#tskel.edges = np.unique(edges, axis=0)
	tskel.nodes = nodes
	tskel.edges = nodes
	
	#print(tskel.nodes)
	#print(tskel.edges)

	# Write to VTP File
	graph = vtkMutableUndirectedGraph()
	points = vtkPoints()
	flag=0
	vertex1=[]
	vertex2=[]
	for node in tskel.nodes:
		if flag == 0:
			vertex1 = graph.AddVertex()
			points.InsertNextPoint(node)
			flag = 1
		else:	
			vertex2 = graph.AddVertex()
			graph.AddEdge (vertex1, vertex2)
			points.InsertNextPoint(node)
			flag = 0

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
	
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".pkl"
	save_skeleton(tskel,outfile)	
	
	#break
'''

				
##### Working Previous #####	
'''	    
for timestep in range(121):
	print("Forward Timestep: "+str(timestep))
	cfile = "./skel/35PSU_Skel_"+str(timestep)+".pkl"
	nfile = "./skel/35PSU_Skel_"+str(timestep+1)+".pkl"
	
	cskel = load_skeleton(cfile)
	nskel = load_skeleton(nfile)
	
	cskel['nodes'] = cskel['nodes'].astype(np.int64)
	nskel['nodes'] = nskel['nodes'].astype(np.int64)
	
	#print(cskel)
	
	tskel = Skeleton()
	nodes = np.array([])
	edges = np.array([])
	first = 0
	
	for c in range(cskel['nodes'].shape[0]):
		cnode = cskel['nodes'][c]
		v1 = c
		v2 = c
		#print(cnode)
		neighbornode = nskel['nodes'][0]
		neighbordist = 1000000
		for n in range(nskel['nodes'].shape[0]):
			nnode = nskel['nodes'][n]
			if cnode[0] != nnode[0] and cnode[1] != nnode[1] and cnode[2] != nnode[2]:
				#print(cnode)
				#print(nnode)
				#print(cnode-nnode)
				#dist = distance(cnode[0],cnode[1],cnode[2],nnode[0],nnode[1],nnode[2])
				dx = (cnode[0]-nnode[0])**2
				dy = (cnode[1]-nnode[1])**2
				dz = (cnode[2]-nnode[2])**2
				#print(dx, dy, dz)
				dist = dx + dy + dz
				#print(int(cnode[0]),int(cnode[1]),int(cnode[2]),int(nnode[0]),int(nnode[1]),int(nnode[2]), dist, neighbordist)
				if dist < neighbordist:
					neighbordist = dist
					neighbornode = nnode
					v2 = n
		
		if first == 0:
			nodes = [cnode]
			nodes = np.concatenate((nodes,[neighbornode]))
			edges = [[cnode], [neighbornode]]
			
			first = 1
		else:		
			nodes = np.concatenate((nodes,[cnode]))
			nodes = np.concatenate((nodes,[neighbornode]))
			edge = [[cnode], [neighbornode]]				
			edges = np.concatenate((edges,edge))
			#break
		
	# Consolidate nodes and edges
	#tskel.nodes = np.unique(nodes, axis=0)
	#tskel.edges = np.unique(edges, axis=0)
	tskel.nodes = nodes
	tskel.edges = nodes
	
	#print(tskel.nodes)
	#print(tskel.edges)

	# Write to VTP File
	graph = vtkMutableUndirectedGraph()
	points = vtkPoints()
	flag=0
	vertex1=[]
	vertex2=[]
	for node in tskel.nodes:
		if flag == 0:
			vertex1 = graph.AddVertex()
			points.InsertNextPoint(node)
			flag = 1
		else:	
			vertex2 = graph.AddVertex()
			graph.AddEdge (vertex1, vertex2)
			points.InsertNextPoint(node)
			flag = 0

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	outfile = "./tracks/forward/35PSU_Track_F_"+"{:03d}".format(timestep)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
	
	outfile = "./tracks/forward/35PSU_Track_F_"+"{:03d}".format(timestep)+".pkl"
	save_skeleton(tskel,outfile)	
	
	#break


for timestep in range(121):
	print("Backward Timestep: "+str(timestep+1))
	cfile = "./skel/35PSU_Skel_"+str(timestep+1)+".pkl"
	nfile = "./skel/35PSU_Skel_"+str(timestep)+".pkl"
	
	cskel = load_skeleton(cfile)
	nskel = load_skeleton(nfile)
	
	cskel['nodes'] = cskel['nodes'].astype(np.int64)
	nskel['nodes'] = nskel['nodes'].astype(np.int64)
	
	#print(cskel)
	
	tskel = Skeleton()
	nodes = np.array([])
	edges = np.array([])
	first = 0
	
	for c in range(cskel['nodes'].shape[0]):
		cnode = cskel['nodes'][c]
		v1 = c
		v2 = c
		#print(cnode)
		neighbornode = nskel['nodes'][0]
		neighbordist = 1000000
		for n in range(nskel['nodes'].shape[0]):
			nnode = nskel['nodes'][n]
			if cnode[0] != nnode[0] and cnode[1] != nnode[1] and cnode[2] != nnode[2]:
				#print(cnode)
				#print(nnode)
				#print(cnode-nnode)
				#dist = distance(cnode[0],cnode[1],cnode[2],nnode[0],nnode[1],nnode[2])
				dx = (cnode[0]-nnode[0])**2
				dy = (cnode[1]-nnode[1])**2
				dz = (cnode[2]-nnode[2])**2
				#print(dx, dy, dz)
				dist = dx + dy + dz
				#print(int(cnode[0]),int(cnode[1]),int(cnode[2]),int(nnode[0]),int(nnode[1]),int(nnode[2]), dist, neighbordist)
				if dist < neighbordist:
					neighbordist = dist
					neighbornode = nnode
					v2 = n
		
		if first == 0:
			nodes = [cnode]
			nodes = np.concatenate((nodes,[neighbornode]))
			edges = [[cnode], [neighbornode]]
			
			first = 1
		else:		
			nodes = np.concatenate((nodes,[cnode]))
			nodes = np.concatenate((nodes,[neighbornode]))
			edge = [[cnode], [neighbornode]]				
			edges = np.concatenate((edges,edge))
			#break
		
	# Consolidate nodes and edges
	#tskel.nodes = np.unique(nodes, axis=0)
	#tskel.edges = np.unique(edges, axis=0)
	tskel.nodes = nodes
	tskel.edges = nodes
	
	#print(tskel.nodes)
	#print(tskel.edges)

	# Write to VTP File
	graph = vtkMutableUndirectedGraph()
	points = vtkPoints()
	flag=0
	vertex1=[]
	vertex2=[]
	for node in tskel.nodes:
		if flag == 0:
			vertex1 = graph.AddVertex()
			points.InsertNextPoint(node)
			flag = 1
		else:	
			vertex2 = graph.AddVertex()
			graph.AddEdge (vertex1, vertex2)
			points.InsertNextPoint(node)
			flag = 0

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
	
	outfile = "./tracks/backward/35PSU_Track_B_"+"{:03d}".format(timestep+1)+".pkl"
	save_skeleton(tskel,outfile)	
	
	#break		

'''		
