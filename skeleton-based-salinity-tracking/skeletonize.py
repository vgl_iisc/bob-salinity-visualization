from skeletonization import *
from utils import *
from dataIO import *
from sys import argv
import networkx as nx

#from paraview.simple import *
import scipy.io as spio

import vtk, vtk.util.numpy_support
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from vtk import vtkPolyDataWriter, vtkMutableUndirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter

'''
from pyevtk.hl import gridToVTK
from vtk import vtkXMLUnstructuredGridReader
from vtk.util import numpy_support
'''

import numpy as np
import pandas as pd

i=0
for timestep in range(0,122):
	infile = "/home/dhipu/Dhipu/Data/latest/35PSU."+str(timestep)+".csv"
	print("Processing: "+infile)
	#df=pd.read_csv('/home/dhipu/Dhipu/Data/35PSU_44.csv', sep=',',names=["so","Result","Points:0","Points:1","Points:2"],header=0)
	df=pd.read_csv(infile, sep=',',names=["so","35PSU","Points:0","Points:1","Points:2"],header=0)

	x_data = df['Points:0']
	y_data = df['Points:1']
	z_data = df['Points:2']
	label_data = df['35PSU']

	# Remove all the points where Result = 0
	print(label_data.shape)
	ldata = np.where(label_data==1)
	print(ldata[0].shape)

	x_data = x_data[ldata[0]]
	y_data = y_data[ldata[0]]
	z_data = z_data[ldata[0]]
	z_data = z_data - np.amin(z_data) 

	#print(x_data.shape, y_data.shape, z_data.shape)


	print(np.amax(x_data), np.amin(x_data))
	print(np.amax(y_data), np.amin(y_data))
	print(np.amax(z_data), np.amin(z_data))

	print(np.amax(x_data)-np.amin(x_data))
	print(np.amax(y_data)-np.amin(y_data))
	print(np.amax(z_data)-np.amin(z_data))

	xdim = int(np.ceil(np.amax(x_data)-np.amin(x_data))) * 12 + 1
	ydim = int(np.ceil(np.amax(y_data)-np.amin(y_data))) * 12 + 1
	zdim = int(np.ceil(np.amax(z_data)-np.amin(z_data))) + 1

	print(xdim,ydim,zdim)

	p = np.zeros((xdim, ydim, zdim), dtype=np.uint8)

	xmin = np.amin(x_data)
	ymin = np.amin(y_data)
	print(xmin,ymin)

	xidx = ((np.floor(x_data) - xmin) * 12) + (np.round((x_data - np.floor(x_data)) * 12))
	print(len(xidx))
	xidx = xidx.astype(int)
	yidx = ((np.floor(y_data) - ymin) * 12) + (np.round((y_data - np.floor(y_data)) * 12))
	yidx = yidx.astype(int)
	zidx = (np.absolute(np.floor(z_data)))
	zidx = zidx.astype(int)


	print(len(xidx), len(yidx), len(xidx))

	#p[xidx,yidx,zidx] = (label_data).astype(int)
	p[xidx,yidx,zidx] = 1
	(xroot,yroot,zroot) = np.nonzero(p)
	rootx = xroot[0]
	rooty = yroot[0]
	rootz = zroot[0]

	root = array2point(p, object_id=1)

	print(len(xroot), len(label_data), label_data.shape)
	print(rootx, rooty, rootz, p[rootx,rooty,rootz])

	# Skeletonize
	skeleton = skeletonize(p, object_id=None, parameters = [10,10]) #, init_root=points[0:len(points)])
	print("Root")
	print(skeleton.root)
	print("Nodes")
	print(skeleton.nodes)
	print(skeleton.nodes.shape)
	print("Edges")
	print(skeleton.edges)
	print(skeleton.edges.shape)
	print("Radii")
	print(np.amax(skeleton.radii))

	output = np.zeros( (xdim, ydim, zdim), dtype=np.uint8)
	xidx = skeleton.nodes[:,0]
	yidx = skeleton.nodes[:,1]
	zidx = skeleton.nodes[:,2]
	output[xidx, yidx, zidx] = 1
	print(output[skeleton.nodes[0][0], skeleton.nodes[0][1], skeleton.nodes[0][2]])	


	# Write to VTP File
	graph = vtkMutableUndirectedGraph()
	points = vtkPoints()
	#v = []
	v = dict()
	for i in range(0,skeleton.nodes.shape[0]):
		if skeleton.nodes[i][2] > 50:
			vertex = graph.AddVertex()
			#v.append(vertex)
			v[i] = vertex
			points.InsertNextPoint(skeleton.nodes[i][0],skeleton.nodes[i][1], skeleton.nodes[i][2])

	print(len(v))

	for i in range(0,skeleton.edges.shape[0]):
		if skeleton.nodes[skeleton.edges[i][0]][2] > 50 and skeleton.nodes[skeleton.edges[i][1]][2] > 50:
			#graph.AddEdge (v[skeleton.edges[i][0]], v[skeleton.edges[i][1]] )
			graph.AddEdge (v[skeleton.edges[i][0]], v[skeleton.edges[i][1]] )

	graph.SetPoints(points)

	graphToPolyData = vtkGraphToPolyData()
	graphToPolyData.SetInputData(graph)
	graphToPolyData.Update()

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData(graphToPolyData.GetOutput())
	outfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep)+".vtp"
	writer.SetFileName(outfile)
	writer.Write()
	
	outfile = "./skel/35PSU_Skel_"+"{:03d}".format(timestep)+".pkl"
	save_skeleton(skeleton,outfile)

