import xarray as xr
import numpy as np
import netCDF4
import os

infile = "woa13_A5B2_s01_04.nc"
i=1
infiles = ""

'''
option = input("Select onw option\n 1. Indian Subcontinent\n 2. BoB\n 3. All\n 4. Manual input\n")
if (int(option) == 1):
	cmnd = "cdo sellonlatbox,50,95,-10,27 "
	#os.system(cmnd)
	output = "ISC.nc"
elif(int(option) == 2):
	cmnd = "cdo sellonlatbox,78,95,3,27 "
	#os.system(cmnd)
	output = "BoB.nc"
elif(int(option) == 3):
	cmnd = "cdo sellonlatbox,78,95,8,27 "
	#os.system(cmnd)
	output = "World.nc"	
else:
	cmnd = "cdo sellonlatbox "
	#os.system(cmnd)
	output = "Manual.nc"

while(i<=12):
	
	arr = infile.split("_")
	if i<10:
		t = "0"+str(i)
	else:
		t = str(i)
	
	infile = arr[0]+"_"+arr[1]+"_"+"s"+t+"_"+arr[3]

	if output != "World.nc" :
		outfile = t+".nc"
		infiles = infiles+t+".nc "
		cmd = cmnd+infile+" "+outfile
		os.system(cmd)
		
	else:
		infiles = infiles+infile+" "
	i+=1

cmnd = "cdo mergetime "+infiles+"new.nc"

os.system(cmnd)

if output != "World.nc" :
	cmnd = "rm "+infiles
	os.system(cmnd)
	
dataDIR = "new.nc"
DS = xr.open_dataset(dataDIR,decode_times=False)
da = DS.s_an

time,depth,lat,lon = da.values.shape
temp = np.copy(da.values)


for t in range(0,time):
  for d in range(0,depth):
    da.values[t][(depth-1)-d] = temp[t][d]

DS.to_netcdf(output)
DS.close()
os.system("rm new.nc")
'''

os.system("cp -f ISC.nc ISC1.nc")

output = "ISC1.nc"
DS = netCDF4.Dataset(output, "a")

'''
new_depth = DS.createVariable("new_depth", np.float32, ("time", "depth", "lat", "lon"))
shape = DS.variables["new_depth"].shape

t=0
temp = np.full((shape[0], shape[1], shape[2], shape[3]), 0)

while(t<shape[0]):
	d=0
	while(d<shape[1]):
		n_depth = DS.variables["depth"][shape[1]-1] - DS.variables["depth"][d]
		t1 = np.full((shape[2], shape[3]), n_depth)
		temp[t][d] = np.copy(t1)
		d+=1
	t+=1
new_depth[:] = temp
'''

new_san = DS.createVariable("new_san", np.float32, ("time", "depth", "lat", "lon"))
san_shape = DS.variables["new_san"].shape
print(san_shape)

t=0
temp = np.full((san_shape[0], san_shape[1], san_shape[2], san_shape[3]), 0)
print(san_shape[0])
n_san = np.around(DS.variables["s_an"], decimals=0)

'''
while(t<san_shape[0]):
	d=0
	while(d<san_shape[1]):
		
		
		#t1 = np.full((san_shape[2], san_shape[3]), n_san)
		#temp[t][d] = np.copy(t1)
		n_san = np.around(n_san, decimals=1)
		#print(n_san)
		d+=1
	t+=1
'''
print(n_san)
new_san[:] = n_san
print("Hello")
DS.close()
#input("Delete?")
#os.system("rm -f ISC1.nc")


