#!/usr/bin/python

import sys
from paraview.simple import *


files = ["/home/vgl/DTM/Project/Data/ISC/woa13_decav_s01.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s02.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s03.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s04.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s05.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s06.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s07.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s08.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s09.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s10.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s11.nc",
		 "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s12.nc"]

								 
reader = NetCDFReader(FileName = "/home/vgl/DTM/Project/Data/ISC/woa13_decav_s08.nc", 
#						ReplaceFillValueWithNan = True, 
						Dimensions = '(depth, lat, lon)', 
						SphericalCoordinates = False)

#Show()
#source = GetActiveSource()



#Transform Filter
source = GetActiveSource()
transformFilter = Transform(Input=source)
#transformFilter.ShowBox = False
transformFilter.Transform.Scale = [1.0, 1.0, 0.01]
transformFilter.Transform.Translate = [0 , 0, 0]
transformFilter.Transform.Rotate = [0 , 0, 0]
transformFilter.TransformAllInputVectors = True
transformFilter.UpdatePipeline()

'''
#Display Properties and Color Map
Show(transformFilter)    
displayProperties = GetDisplayProperties(transformFilter)
displayProperties.Representation='Volume'
dataRange = transformFilter.PointData.GetArray('s_an').GetRange()
displayProperties.LookupTable = MakeBlueToRedLT(dataRange[0], dataRange[1])
displayProperties.ColorArrayName = 's_an'
Render()
'''

#Contour Filter
contourFilter = Contour(transformFilter)
contourFilter.ContourBy = 's_an'
contourFilter.ComputeNormals = True
contourFilter.ComputeGradients = False
contourFilter.GenerateTriangles = True
contourFilter.Isosurfaces.ValueRange = [34.8, 35.2, 10]
contourFilter.UpdatePipeline()

#Display Properties and Color Map 
Show(contourFilter)
displayProperties = GetDisplayProperties(contourFilter)
displayProperties.Representation='Surface'
dataRange = contourFilter.PointData.GetArray('s_an').GetRange()
displayProperties.LookupTable = MakeBlueToRedLT(dataRange[0], dataRange[1])
displayProperties.ColorArrayName = 's_an'  
displayProperties.Opacity = 1.0
print(displayProperties.ListProperties()) 
Render()



#Camera Parameters
camera=GetActiveCamera()
camera.SetPosition(85.5,13.75,51.2717)
camera.SetFocalPoint(85.5,13.75,-1)
camera.SetViewUp(0,1,0)
camera.SetViewAngle(30.00)
camera.SetEyeAngle(20.00)
camera.SetFocalDisk(1.00)
camera.SetFocalDistance(0.00)
camera.Zoom(1.5)
Render()


'''
#View Parameters
view = GetRenderView()
view.ViewSize = [800, 600]
Render()
'''


#Show()
#Render()
		 

while True:
	option = input("Type 'Exit' followed by 'Enter' key, to exit\n")
	if option == 'Exit':
		sys.exit(0)
