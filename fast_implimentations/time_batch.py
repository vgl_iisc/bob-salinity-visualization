import time
start=time.time()

st = time.time()

import sys
import gc
gc.collect()

n = len(sys.argv)

if n!=4:
	print('BAD ARGUMENT ERROR')
	exit()

DIR = sys.argv[1]
T = sys.argv[2]
INIT_T = sys.argv[3]

T=int(T)
INIT_T=int(INIT_T)

from mpi4py import MPI

comm = MPI.COMM_WORLD
t = int(comm.Get_rank())

import numpy as np
import itertools
import netCDF4 as nc

import os
from scipy import ndimage
import cc3d

fin = open(DIR+'/parameter.txt', 'r')
Lines = fin.readlines()

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())
		#print(parameters)

indir = str(parameters[3])
infile = str(parameters[4])

v_limit = int(parameters[0])

lt_clip = int(parameters[1])
ln_clip = int(parameters[2])

interpolation = int(parameters[8])

d_max = int(parameters[6])

neighborhood = int(parameters[7])

slt_ = str(parameters[9])
dep_ = str(parameters[10])
lat_ = str(parameters[11])
lon_ = str(parameters[12])

print(DIR+'/'+indir+'/'+infile+'_'+str(INIT_T+t)+'.nc')
DS = nc.Dataset(DIR+'/'+indir+'/'+infile+'_'+str(INIT_T+t)+'.nc','r')

lt_origin = DS.variables[lat_][0]
ln_origin = DS.variables[lon_][0]

resolution =(DS.variables[lat_][1])-(DS.variables[lat_][0])

nbrhd = int(neighborhood/(resolution*111))

if nbrhd%2==0:
	nbrhd+=1

lt_clip = round((lt_clip-lt_origin)/resolution)
ln_clip = round((ln_clip-ln_origin)/resolution)

def euc_distance(p1,p2):
	dist = 0
	for i in range(len(p1)):
		diff = p1[i]-p2[i]
		dist += (diff**2)
	return np.sqrt(dist)

salt_ = np.copy(DS.variables[slt_]).astype(np.float32)


shape = salt_.shape

depths_ = np.abs(np.copy(DS.variables[dep_])).astype(np.uint16)

print('data_loading',(time.time()-st)/60,INIT_T+t)
st=time.time()

iso_vol = np.zeros((d_max+1,shape[2],shape[3])).astype(np.float32)

d_c = depths_[0]
for i in range(0,d_c):
	iso_vol[i,:,:]=salt_[0,0,:,:]

for d in range(shape[1]):
	d_c = depths_[d]
	if d_c>d_max:
		break
	iso_vol[d_c,:,:]=salt_[0,d,:,:]

#for d in range(shape[1]):
#	d_c = depths_[d]
#	if d==0 and d_c!=0:
#		for i in range(0,d_c):
#			iso_vol[i,:,:]=salt_[0,d,:,:]	
#	if d_c>d_max:
#		break
#	iso_vol[d_c,:,:]=salt_[0,d,:,:]

for d in range(shape[1]-1):
	d_1 = depths_[d]
	d_2 = depths_[d+1]
	for d_c in range(d_1+1,d_2):
		if d_c>d_max:
			break
		#v_mx = salt_[0,d]
		#v_mn = salt_[0,d+1]
		alpha = (d_2-d_c) / (d_2-d_1)
		#val = ((alpha) * v_mn + (1-alpha) * v_mx)
		#iso_vol[d_c] = val
		iso_vol[d_c] = ((alpha) * salt_[0,d+1] + (1-alpha) * salt_[0,d])
		
iso_vol[np.isnan(iso_vol)]=0
iso_vol[iso_vol<v_limit]=0
iso_vol[iso_vol>0]=1

del salt_, depths_
gc.collect()

print('depth_interpolation',(time.time()-st)/60,INIT_T+t)
st=time.time()

fronts=np.zeros((d_max,shape[2],shape[3])).astype(np.uint16)

for d in range(d_max):
	avg = ndimage.uniform_filter(np.pad(iso_vol[d],1), size=3)
	avg = avg[1:shape[2]+1,1:shape[3]+1]

	avg[avg<=(1/9)]=0
	avg[avg==1]=0
	avg[avg>0]=1
	
	#avg = avg+1
	#avg[avg<2] = 1
	#avg[avg>1] = 0

	bound = np.multiply(iso_vol[d],avg).astype(np.uint16)
	
	del avg
	
	ex_comps, n_comps = cc3d.connected_components(bound,return_N=True,connectivity = 8)
	del bound
	gc.collect()
	#ex_comps = ex_comps.astype(np.uint16)
	
	east=[]
	west=[]
	
	for uid in range(1, n_comps+1):
		lt__,ln__ = np.where(ex_comps==uid)
		comp = zip(lt__,ln__)
		(lte,lne) = (shape[2],0)
		(ltw,lnw) = (shape[2],shape[3])
		for (lt,ln) in comp:
			
			if ln>lne or (ln==lne and lt<lte):
				(lte,lne) = (lt,ln)
			if ln<lnw or (ln==lnw and lt<ltw):
				(ltw,lnw) = (lt,ln)
		east.append((lte,lne))
		west.append((ltw,lnw))
		
	lst = zip(west,east)
	for (src,dst) in lst:
		(lt, ln) = src
		(ltr, lnr) = (lt-1, ln-1)
		stop = (ltr, lnr)
		fronts[d,lt,ln] = ex_comps[lt,ln]
		while((lt,ln)!=dst):
			if ltr>lt and lnr<=ln:
				lnr+=1
			elif lnr>ln and ltr>=lt:
				ltr-=1
			elif ltr<lt and lnr>=ln:
				lnr-=1
			elif lnr<ln and ltr<=lt:
				ltr+=1
			
			if ltr<0 or ltr>shape[2]-1 or lnr<0 or lnr>shape[3]-1:
				continue
			
			if ex_comps[ltr,lnr]==ex_comps[lt,ln]:
				fronts[d,ltr,lnr]=ex_comps[ltr,lnr]
				(lt,ln), (ltr,lnr) = (ltr,lnr), (lt,ln)
				stop = (ltr, lnr)
				continue
				
			if (ltr,lnr) == stop:
				input('ERROR in fronts')		
				break
				
del iso_vol
gc.collect()

half_mask = np.zeros((nbrhd, nbrhd)).astype((np.uint16))
lt_ = np.arange(nbrhd)
for lt,ln in itertools.product(lt_,lt_):
	if ((nbrhd//2-lt)**2+(nbrhd//2-ln)**2)<=((nbrhd//2)**2):
		half_mask[lt,ln]=1

fronts[fronts>0]=1
fronts2 = np.pad(fronts, pad_width=((0,0),(nbrhd//2,nbrhd//2),(nbrhd//2,nbrhd//2)), mode='constant', constant_values=0)	

d__,lt__,ln__ = np.nonzero(fronts2)
lst=zip(d__,lt__,ln__)

for (d1,lt1,ln1) in lst:
	fronts2[d1, lt1-(nbrhd//2):lt1+(nbrhd//2)+1, ln1-(nbrhd//2):ln1+(nbrhd//2)+1] += half_mask

del half_mask
gc.collect()

sh = fronts2.shape
fronts2 = fronts2[:,nbrhd//2:sh[1]-(nbrhd//2),nbrhd//2:sh[2]-(nbrhd//2)]
fronts2[fronts2>0]=1
		
fronts2, n_comps = cc3d.connected_components(fronts2,return_N=True,connectivity = 26)

#print('no of comps', n_comps, INIT_T+t)

fronts = np.multiply(fronts2,fronts)
del fronts2
gc.collect()

fronts[:,:,0:ln_clip+1]=0
fronts[:,0:lt_clip+1,:]=0

cluster_rep = dict()
fronts_dict=dict()

(d__,lt__,ln__)=np.nonzero(fronts)
lst = zip(d__,lt__,ln__)

for (d,lt,ln) in lst:
	label = fronts[d,lt,ln]
	fronts_dict[t,d,lt,ln] = label
	if cluster_rep.get(label):
		cluster_rep[label][1]+=d
		cluster_rep[label][2]+=lt
		cluster_rep[label][3]+=ln
		cluster_rep[label][4]+=1
	else:
		cluster_rep[label]=[t,d,lt,ln,1]
		
cluster_rep_new=dict()

for label, [t1,d1,lt1,ln1,ct1] in cluster_rep.items():
	cluster_rep[label] = [t1,d1/ct1,lt1/ct1,ln1/ct1,ct1,float('inf')]
	cluster_rep_new[label]= [t1,d1/ct1,lt1/ct1,ln1/ct1,ct1]

(d__,lt__,ln__)=np.nonzero(fronts)
lst = zip(d__,lt__,ln__)
for (d,lt,ln) in lst:
	label = fronts[d,lt,ln]
	if cluster_rep.get(label):
		(t1,d1,lt1,ln1,ct1,dist1) = cluster_rep[label]
	else:
		continue
	dist = euc_distance((d,lt,ln),(d1,lt1,ln1))
	if dist<dist1:
		cluster_rep_new[label] = [t,d,lt,ln,ct1]
		cluster_rep[label][5]=dist

del fronts
gc.collect()

print('surface_front_computation',(time.time()-st)/60,INIT_T+t)
st=time.time()

np.save(DIR+'/cluster_rep/cluster_rep'+str(INIT_T+t)+'.npy', cluster_rep_new)
np.save(DIR+'/fronts/surface_fronts_'+str(INIT_T+t)+'.npy', fronts_dict)

temp=0
np.save(DIR+'/fronts/temp_'+str(INIT_T+t)+'.npy', temp)


print('surface_front_storage',(time.time()-st)/60,INIT_T+t)
st=time.time()

if INIT_T+t>=T-1:
	#print('total_time',(time.time()-start)/60, INIT_T+t)
	np.save(DIR+'/done_'+str(INIT_T+t)+'.npy', T)
	exit()

while('True'):
	if os.path.isfile(DIR+'/fronts/temp_'+str(INIT_T+t+1)+'.npy'):
		nxt_fronts_dict = np.load(DIR+'/fronts/surface_fronts_'+str(INIT_T+t+1)+'.npy', allow_pickle=True).item()
		break
	else:
		#time.sleep(10)
		continue

print('wait_time',(time.time()-st)/60,INIT_T+t)
st=time.time()

full_mask = np.zeros((2,2*nbrhd+1, 2*nbrhd+1)).astype((np.uint16))

lt_ = np.arange(2*nbrhd+1)
d_=np.arange(2)

for d,lt,ln in itertools.product(d_,lt_,lt_):
	if ((nbrhd-lt)**2+(nbrhd-ln)**2)<=((nbrhd)**2):
		full_mask[d,lt,ln]=1

edges = dict()

fronts=np.zeros((d_max,shape[2],shape[3])).astype(np.uint16)
nxt_fronts=np.zeros((d_max,shape[2],shape[3])).astype(np.uint16)

for (t_,d,lt,ln) , label in fronts_dict.items():
	fronts[d,lt,ln] = label
	
for (t_,d,lt,ln) , label in nxt_fronts_dict.items():
	nxt_fronts[d,lt,ln] = label

del fronts_dict, nxt_fronts_dict
gc.collect()

fronts = np.pad(fronts, pad_width=((1,1),(nbrhd,nbrhd),(nbrhd,nbrhd)), mode='constant', constant_values=0)

nxt_fronts = np.pad(nxt_fronts, pad_width=((1,1),(nbrhd,nbrhd),(nbrhd,nbrhd)), mode='constant', constant_values=0)

d_,lt_, ln_ = np.nonzero(fronts)
lst = zip(d_,lt_,ln_)
for d,lt,ln in lst:
	src = fronts[d,lt,ln]
	temp1 = np.multiply(nxt_fronts[d:d+2,lt-nbrhd:lt+nbrhd+1,ln-nbrhd:ln+nbrhd+1],full_mask)
	temp1 = temp1[np.nonzero(temp1)]
	
	dst_lst = set(temp1)
	
	if edges.get(src):
		edges[src] = edges[src].union(dst_lst)
	else:
		edges[src] = dst_lst

print('edge_comutation',(time.time()-st)/60,INIT_T+t)
st=time.time()

np.save(DIR+'/edges/edges_'+str(INIT_T+t)+'.npy', edges)
del edges
gc.collect()

#os.system('rm '+DIR+'/fronts/temp_'+str(INIT_T+t)+'.npy')

print('edge_storage',(time.time()-st)/60,INIT_T+t)

#print('total_time',(time.time()-start)/60, INIT_T+t)

np.save(DIR+'/T.npy', T)
np.save(DIR+'/done_'+str(INIT_T+t)+'.npy', T)
