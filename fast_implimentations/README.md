## parameter.txt:
	
	Contains parameter values required by other scrpts. Change if needed, otherwise the default values are already set.

---

## sbatch_sch.sh:
	
	SBATCH file to schedule batches of parallel processes of "time_batch.py" over a SLURM cluster using MPI.

	To Execute: run "sbatch -n NP sbatch_sch.sh FR_T TO_T+1 PATH"
	
	Here NP is number of processes to be scheduled starting at time step FR_T and ending at time step TO_T and PATH is exact path to the parent directory where data is stored.
	ex : "sbatch -n 122 sbatch_sch.sh 0 122 \scratch\upkarsingh\"
	
---

## time_batch.py:
	
	Computes front based cluster(surface frotns) and the edges for these fronts from a time step 't' to 't+1'. It is time independent code and is initialised by "sbatch_sch.sh" in form of batches. Requires parameter.txt for various parameter values.

	To Execute: use sbatch instruction from instructions given for "sbatch_sch.sh"

---
