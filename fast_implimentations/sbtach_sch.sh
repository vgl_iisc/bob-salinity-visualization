#!/bin/sh
#SBATCH --job-name=front_based # Job name
#SBATCH --time=10:00:00 # Time limit hrs:min:sec
#SBATCH -o OUTPUT.txt
#SBATCH -e ERROR.txt
#SBATCH --partition=debug 
pwd; hostname; date

p=$(pwd);

module load openmpi
module load conda

mpirun python3 time_batch.py $3 $2 $1
