## parameter.txt:
	
	Contains parameter values required by other scrpts. Change if needed, otherwise the default values are already set.

---

## TrackGraph.py:
	
	Computes the track graph and output track graph can be visualised using the paraview state file named TG_VIS.pvsm.

	To Execute: run "python3 TrackGraph.py"
	
---

## LongPath.py:
	
	Computes long paths clustered with source and destination surface front. Clustering matrix is a parameter in parameter.txt. Output can be visualised my using the paraview state file named path_box.pvsm

	To Execute: run "python3 LongPath.py"

---

## TG_VIS.pvsm:
	
	Usage:
		1. Open paraview
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.

---

## path_box.pvsm:

	Usage:
		1. Open paraview
		2. Select load state option in file menu.
		3. Load this file.
		4. Dialogue box "Load state options" will open, Select "Search files under specific directory" as the Load state data file option.
		5. Select the directory of frond based code and press OK.

---
