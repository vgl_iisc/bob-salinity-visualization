#new_track_mult
import itertools
import numpy as np
import netCDF4
import os
import vtk
from vtk import vtkPolyDataWriter, vtkMutableDirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter
import time
import math
from scipy import ndimage
import multiprocessing as mp
import matplotlib.pyplot as plt
import datetime
import networkx as nx

fin = open('parameter.txt', 'r')
Lines = fin.readlines()

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())
		#print(parameters)

infile = str(parameters[5])

filename = str(parameters[6])

v_limit = int(parameters[0])

lt_origin = int(parameters[1])
ln_origin = int(parameters[2])

lt_clip = int(parameters[3])
ln_clip = int(parameters[4])

interpolation = int(parameters[10])

d_max = int(parameters[7])

resolution = float(parameters[8].split('/')[0].strip())/float(parameters[8].split('/')[1].strip())

neighborhood = int(parameters[9])

v_name = str(parameters[11])

d_name = str(parameters[12])

nbrhd = int(neighborhood/(resolution*111))

n_paths = int(parameters[15])

n_paths = 1

set_bound_ln=[]
set_bound_lt=[]

bnds = parameters[13].split(',')
for i in bnds:
	set_bound_ln.append(float(i.strip()))

bnds = parameters[14].split(',')
for i in bnds:
	set_bound_lt.append(float(i.strip()))

set_bound_lt = [lt_origin]+set_bound_lt
set_bound_ln = [ln_origin]+set_bound_ln

lt_sets = [t for t in range(len(set_bound_lt))]
ln_sets = [t for t in range(len(set_bound_ln))]

def distance(p1, p2):
	x_d = p2[0]-p1[0]
	y_d = p2[1]-p1[1]
	#return math.sqrt((x_d*x_d)+((y_d*y_d)**5))
	#return abs(y_d)
	return y_d

def euc_dist(p1, p2):
	x_d = p2[0]-p1[0]
	y_d = p2[1]-p1[1]
	z_d = p2[2]-p1[2]
	return math.sqrt((x_d**2)+(y_d**2)+(z_d**2))
'''
shape = [d_max, 0, 0, 0]

for d in range(shape[0]):
	k_vd = np.load('vol/vol_s'+str(d)+'.npy', allow_pickle=True).item()
	for (d_, t, lt, ln) in k_vd:
		if t+1>shape[1]:
			shape[1] = t+1
		if lt+1>shape[2]:
			shape[2] = lt+1
		if ln+1>shape[3]:
			shape[3] = ln+1
'''

shape = [d_max, 122, 241, 253]

cluster_rep = []
for t in range(shape[1]):
	CR = np.load('vol/cluster_rep'+str(t)+'.npy', allow_pickle=True).item()
	cluster_rep.append(CR)

set_bound_lt = set_bound_lt+[lt_origin+(shape[2]*resolution)+1]
set_bound_ln = set_bound_ln+[ln_origin+(shape[3]*resolution)+1]

set_blt = [(lt-lt_origin)/resolution for lt in set_bound_lt]
set_bln = [(ln-ln_origin)/resolution for ln in set_bound_ln]

print(shape)

wt_fact = distance([0,0], [shape[3], shape[2]])

print('paths', n_paths)

start = time.time()

graph = nx.DiGraph()
'''
max_ct = 0
for d in range(shape[0]):
	edges_d = np.load('edg_clipped/edges'+str(d)+'.npy', allow_pickle=True).item()
	for e, ct in edges_d.items():
		if max_ct<ct:
			max_ct=ct
	del edges_d

print(max_ct, 'max_ct')
'''
for t in range(shape[1]-1):
	for d in range(shape[0]):
		edges = np.load('edges/edges'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		if len(edges)==0:
			continue
		for (p1,p2) in edges:
			p1 = (int(p1[0]), int(p1[1]))
			p2 = (int(p2[0]), int(p2[1]))
			(t1,cn1) = p1
			(t2,cn2) = p2
			if not cluster_rep[t].get(cn1): continue
			if not cluster_rep[t+1].get(cn2): continue
			graph.add_edge(p1, p2)#, weight=wt_fact-distance([x1,y1,d1], [x2,y2,d2]) )
		del edges

def path_weight(path):
	N = len(path)
	weight = 0
	for n in range(N-1):
		s1 = path[n]
		s2 = path[n+1]
		weight += graph.get_edge_data(s1,s2)['weight']
	return math.sqrt(weight)

in_d = graph.in_degree()
out_d = graph.out_degree()

src_lst = []
for i in in_d:
	if i[1]==0:
		src_lst.append(i[0])

dst_lst=[]
for i in out_d:
	if i[1]==0:
		dst_lst.append(i[0])

src_len = len(src_lst)
print(src_len, 'srcs')
parts = (os.cpu_count())
step = int(src_len/parts)+1

def foo(i):
	far_pair_lst = {}
	dist_lst = {}
	dst_app = {}
	for n_ct in itertools.product(lt_sets,ln_sets):
		for n_ct2 in itertools.product(lt_sets,ln_sets):
			for p_count in range(n_paths):

				far_pair = []
				far_pair.append([src_lst[i], src_lst[i]])
				far_pair.append([])

				for pi in range(i*step, (i+1)*step):
					if not(pi<src_len):
						break
					pt = src_lst[pi]
					[tp, dp, yp, xp, _] = cluster_rep[pt[0]][pt[1]]

					far_dst = pt
					pts_dst = nx.single_source_dijkstra_path(graph, pt)
					#keys = pts_dst.keys()
					for dst in dst_lst:
						temp_pair = tuple([pt,dst])

						if pts_dst.get(dst):
							if far_pair_lst.get(temp_pair):
								continue
							if dst_app.get(dst):
								continue

							[td, dd, yd, xd,_] = cluster_rep[dst[0]][dst[1]]
							[tf, df, yf, xf,_] = cluster_rep[far_dst[0]][far_dst[1]]

							if yd>=set_blt[n_ct2[0]+1]  or yd<set_blt[n_ct2[0]] :
								continue
							if xd>=set_bln[n_ct2[1]+1]  or xd<set_bln[n_ct2[1]] :
								continue

							if yp>=set_blt[n_ct[0]+1]  or yp<set_blt[n_ct[0]] :
								continue
							if xp>=set_bln[n_ct[1]+1]  or xp<set_bln[n_ct[1]] :
								continue

							path_dst = pts_dst[dst]
							path_far_dst = pts_dst[far_dst]

							dist_p_d = distance([xp, yp], [xd, yd]) #+ weight_dst
							dist_p_f = distance([xp, yp], [xf, yf]) #+ weight_far_dst

							if dist_lst.get(dist_p_d):
								continue

							if dist_p_d > dist_p_f:
								far_dst = dst

					[tf, df, yf, xf,_] = cluster_rep[far_dst[0]][far_dst[1]]
					[ts, ds, ys, xs,_] = cluster_rep[far_pair[0][0][0]][far_pair[0][0][1]]
					[td, dd, yd, xd,_] = cluster_rep[far_pair[0][1][0]][far_pair[0][1][1]]

					dist_s_d = distance([xs, ys], [xd, yd])
					dist_p_f = distance([xp, yp], [xf, yf])

					if dist_p_f > dist_s_d:
						far_pair[0][0] = pt
						far_pair[0][1] = far_dst
						far_pair[1] = pts_dst[far_dst]

				far_pair_lst[tuple(far_pair[0])] = far_pair[1]
				[ts, ds, ys, xs,_] = cluster_rep[far_pair[0][0][0]][far_pair[0][0][1]]
				[td, dd, yd, xd,_] = cluster_rep[far_pair[0][1][0]][far_pair[0][1][1]]
				dist_s_d = distance([xs, ys], [xd, yd])

				dist_lst[dist_s_d] = 1
				dst_app[far_pair[0][1]] = 1

	return far_pair_lst

'''
for i in range(src_len):

	foo(i)
'''
pool = mp.Pool()
far_paths = pool.map(foo, np.arange(parts))
pool.close()
pool.join()
del pool

far_dict = {}

for temp_dict in far_paths:
	for key, value in temp_dict.items():
		far_dict[key] = value

del far_paths

print('again')

far_pair_lst = {}
dist_lst = {}
dst_app = {}
for n_ct in itertools.product(lt_sets,ln_sets):
	for n_ct2 in itertools.product(lt_sets,ln_sets):
		for p_count in range(n_paths):

			lp = []
			min = cluster_rep[0].keys()
			min = np.copy(list(min)	)
			min = np.min(min)
			lp.append(tuple([(0,min),(0,min)]))
			lp.append([])
			for key, value in far_dict.items():
				far_pair = [key, value]
				if far_pair_lst.get(far_pair[0]):
					#print(far_pair[0], ' already present')
					continue
				if dst_app.get(far_pair[0][1]):
					continue

				[tp, dp, yp, xp,_] = cluster_rep[lp[0][0][0]][lp[0][0][1]]
				[tf, df, yf, xf,_] = cluster_rep[lp[0][1][0]][lp[0][1][1]]
				[ts, ds, ys, xs,_] = cluster_rep[far_pair[0][0][0]][far_pair[0][0][1]]
				[td, dd, yd, xd,_] = cluster_rep[far_pair[0][1][0]][far_pair[0][1][1]]

				if yd>=set_blt[n_ct2[0]+1]  or yd<set_blt[n_ct2[0]] :
					continue
				if xd>=set_bln[n_ct2[1]+1]  or xd<set_bln[n_ct2[1]] :
					continue

				if ys>=set_blt[n_ct[0]+1]  or ys<set_blt[n_ct[0]] :
					continue
				if xs>=set_bln[n_ct[1]+1]  or xs<set_bln[n_ct[1]] :
					continue

				dist_s_d = distance([xs, ys], [xd, yd])
				dist_p_f = distance([xp, yp], [xf, yf])

				if dist_lst.get(dist_s_d):
					continue

				if dist_s_d > dist_p_f:
					lp[0] = far_pair[0]
					lp[1] = far_pair[1]

			far_pair_lst[lp[0]] = lp[1]
			[ts, ds, ys, xs,_] = cluster_rep[lp[0][0][0]][lp[0][0][1]]
			[td, dd, yd, xd,_] = cluster_rep[lp[0][1][0]][lp[0][1][1]]
			dist_s_d = distance([xs, ys], [xd, yd])

			dist_lst[dist_s_d] = 1
			dst_app[lp[0][1]] = 1

#final_clusters = np.load('final_clusters_clipped.npy', allow_pickle=True)
#print('##########loaded##########')

i_num = 0
p_num = 0

os.system('rm path_cluster_*')

os.system('rm -r adv_fronts')
os.system('mkdir adv_fronts')

os.system('rm -r paths')
os.system('mkdir paths')

ordered=[]

for n_ct in itertools.product(lt_sets,ln_sets):
	for n_ct2 in itertools.product(lt_sets,ln_sets):
		flag=0
		i_num+=1
		lp1 = []
			
		graph_vtk = vtkMutableDirectedGraph()

		points = vtkPoints()

		weights = vtk.vtkDoubleArray()
		weights.SetNumberOfComponents(1)
		weights.SetName('Destination')

		weights1 = vtk.vtkDoubleArray()
		weights1.SetNumberOfComponents(1)
		weights1.SetName('Time')

		weights4 = vtk.vtkDoubleArray()
		weights4.SetNumberOfComponents(1)
		weights4.SetName('Cluster Size')

		#np.save('paths_all_clipped.npy', far_pair_lst)

		for key, value in far_pair_lst.items():
			p_num+=1
			lp = [key, value]
			size = len(lp[1])
			
			#print(key)
			[_,_,yd,xd,_] = cluster_rep[key[1][0]][key[1][1]]
			[_,_,ys,xs,_] = cluster_rep[key[0][0]][key[0][1]]

			if yd>=set_blt[n_ct2[0]+1]  or yd<set_blt[n_ct2[0]] :
				continue
			if xd>=set_bln[n_ct2[1]+1]  or xd<set_bln[n_ct2[1]] :
				continue

			if ys>=set_blt[n_ct[0]+1]  or ys<set_blt[n_ct[0]] :
				continue
			if xs>=set_bln[n_ct[1]+1]  or xs<set_bln[n_ct[1]] :
				continue

			flag = 1

			for n in range(size-1):
				s1_n = lp[1][n]
				s2_n = lp[1][n+1]

				[t1, d1, y1, x1, cs1] = cluster_rep[s1_n[0]][s1_n[1]]
				[t2, d2, y2, x2, cs2] = cluster_rep[s2_n[0]][s2_n[1]]

				x1 = ln_origin+(x1*resolution)
				y1 = lt_origin+(y1*resolution)

				x2 = ln_origin+(x2*resolution)
				y2 = lt_origin+(y2*resolution)

				lp1.append([t1,d1,y1,x1])
				if n==size-2: lp1.append([t2,d2,y2,x2])
				
				vertex1 = graph_vtk.AddVertex()
				points.InsertNextPoint([x1, y1, -d1])
				weights.InsertNextValue(key[1][0]*shape[1]+key[1][1])
				weights1.InsertNextValue(t1)
				weights4.InsertNextValue(cs1)
				vertex2 = graph_vtk.AddVertex()
				points.InsertNextPoint([x2, y2, -d2])
				graph_vtk.AddEdge (vertex1, vertex2)
				weights.InsertNextValue(key[1][0]*shape[1]+key[1][1])
				weights1.InsertNextValue(t2)
				weights4.InsertNextValue(cs2)

			for n in range(size):
				s1_n = lp[1][n]

				graph_vtk1 = vtkMutableDirectedGraph()

				points1 = vtkPoints()

				weights2 = vtk.vtkDoubleArray()
				weights2.SetNumberOfComponents(1)
				weights2.SetName('Destination')

				weights3 = vtk.vtkDoubleArray()
				weights3.SetNumberOfComponents(1)
				weights3.SetName('Time')

				for d in range(d_max):
					adv_frnt = np.copy(np.load('points/adv_frnt'+str(d)+'_'+str(s1_n[0])+'.npy', allow_pickle=True)).astype('int32')
					lt__,ln__ = np.where(adv_frnt==s1_n[1])
					lst = zip(lt__,ln__)
					for (y1, x1) in lst:
						x1 = ln_origin+(x1*resolution)
						y1 = lt_origin+(y1*resolution)

						vertex1 = graph_vtk1.AddVertex()
						points1.InsertNextPoint([x1, y1, -d])
						weights2.InsertNextValue(key[1][0]*shape[1]+key[1][1])
						weights3.InsertNextValue(s1_n[0])

						vertex2 = graph_vtk1.AddVertex()
						points1.InsertNextPoint([x1, y1, -d])
						graph_vtk1.AddEdge (vertex1, vertex2)
						weights2.InsertNextValue(key[1][0]*shape[1]+key[1][1])
						weights3.InsertNextValue(s1_n[0])

				graph_vtk1.SetPoints(points1)
				graph_vtk1.GetVertexData().AddArray(weights2);
				graph_vtk1.GetVertexData().AddArray(weights3);

				graphToPolyData = vtkGraphToPolyData()
				graphToPolyData.SetInputData(graph_vtk1)
				graphToPolyData.Update()
				writer = vtkXMLPolyDataWriter()
				writer.SetInputData(graphToPolyData.GetOutput())

				writer.SetFileName('adv_fronts/'+str(i_num)+'_'+str(key[1][0]*shape[1]+key[1][1])+'_'+str(n)+'.vtp')

				writer.Write()
				del graph_vtk1
				del points1
				del weights2
				del weights3
				del writer
				del graphToPolyData

		if flag==1:
			graph_vtk.SetPoints(points)
			graph_vtk.GetVertexData().AddArray(weights);
			graph_vtk.GetVertexData().AddArray(weights1);

			graphToPolyData = vtkGraphToPolyData()
			graphToPolyData.SetInputData(graph_vtk)
			graphToPolyData.Update()
			writer = vtkXMLPolyDataWriter()
			writer.SetInputData(graphToPolyData.GetOutput())

			writer.SetFileName('path_cluster_new'+str(i_num)+'.vtp')
			ordered.append(lp1)

			writer.Write()
			del graph_vtk
			del points
			del weights
			del weights1
			del weights4
			del writer
			del graphToPolyData


		flag = 0

		graph_vtk = vtkMutableDirectedGraph()
		graph_vtk1 = vtkMutableDirectedGraph()
		
		points = vtkPoints()

		weights = vtk.vtkDoubleArray()
		weights.SetNumberOfComponents(1)
		weights.SetName('Destination')

		weights1 = vtk.vtkDoubleArray()
		weights1.SetNumberOfComponents(1)
		weights1.SetName('Time')

		weights2 = vtk.vtkDoubleArray()
		weights2.SetNumberOfComponents(1)
		weights2.SetName('Cluster Size')

		def smooth(lst):
			flag = 1
			size = len(lst)
			ct = 0
			while(flag==1):
				ct+=1
				flag=0
				if(ct>size):#n_paths):
					break
				for ind in range(1, size-1):
					if lst[ind]>lst[ind-1] and lst[ind]>lst[ind+1]:
							t_dist = 2*lst[ind]-lst[ind-1]-lst[ind+1]
							w1 = (lst[ind]-lst[ind-1])/t_dist
							w2 = (lst[ind]-lst[ind+1])/t_dist
							lst[ind] = (w2*lst[ind-1])+(w1*lst[ind+1])
							flag=1
					elif lst[ind]<lst[ind-1] and lst[ind]<lst[ind+1]:
							t_dist = lst[ind-1]+lst[ind+1]-2*lst[ind]
							w1 = (lst[ind-1]-lst[ind])/t_dist
							w2 = (lst[ind+1]-lst[ind])/t_dist
							lst[ind] = (w2*lst[ind-1])+(w1*lst[ind+1])
							flag=1
			return lst

		for key, value in far_pair_lst.items():
			lp = [key, value]
			size = len(lp[1])

			xData = np.zeros((size))
			yData = np.zeros((size))
			zData = np.zeros((size))
			tData = np.zeros((size))
			csData = np.zeros((size))

			xMin = 10000

			for n in range(size):
				s1_n = lp[1][n]

				[t1, d1, y1, x1, cs1] = cluster_rep[s1_n[0]][s1_n[1]]

				if x1<xMin: xMin = x1
				xData[n] = x1
				yData[n] = y1
				zData[n] = d1
				tData[n] = t1
				csData[n] = cs1

			xData = smooth(xData)
			yData = smooth(yData)
			zData = smooth(zData)

			[_,_,yd,xd,_] = cluster_rep[key[1][0]][key[1][1]]
			[_,_,ys,xs,_] = cluster_rep[key[0][0]][key[0][1]]

			if yd>=set_blt[n_ct2[0]+1]  or yd<set_blt[n_ct2[0]] :
				continue
			if xd>=set_bln[n_ct2[1]+1]  or xd<set_bln[n_ct2[1]] :
				continue

			if ys>=set_blt[n_ct[0]+1]  or ys<set_blt[n_ct[0]] :
				continue
			if xs>=set_bln[n_ct[1]+1]  or xs<set_bln[n_ct[1]] :
				continue

			flag = 1

			for n in range(size-1):
			
				if n==0:
					t_st=0
					
				x1 = xData[n]
				y1 = yData[n]
				d1 = zData[n]
				t1 = tData[n]
				cs1 = csData[n]

				x2 = xData[n+1]
				y2 = yData[n+1]
				d2 = zData[n+1]
				t2 = tData[n+1]
				cs2 = csData[n+1]

				x1 = ln_origin+(x1*resolution)
				y1 = lt_origin+(y1*resolution)

				x2 = ln_origin+(x2*resolution)
				y2 = lt_origin+(y2*resolution)

				vertex1 = graph_vtk.AddVertex()
				points.InsertNextPoint([x1, y1, -d1])
				weights.InsertNextValue(key[1][0]*shape[1]+key[1][1])
				weights1.InsertNextValue(t1)
				weights2.InsertNextValue(cs1)

				vertex2 = graph_vtk.AddVertex()
				points.InsertNextPoint([x2, y2, -d2])
				graph_vtk.AddEdge (vertex1, vertex2)
				weights.InsertNextValue(key[1][0]*shape[1]+key[1][1])
				weights1.InsertNextValue(t2)
				weights2.InsertNextValue(cs2)

		if flag==1:

			graph_vtk.SetPoints(points)
			graph_vtk.GetVertexData().AddArray(weights);
			graph_vtk.GetVertexData().AddArray(weights1);

			graphToPolyData = vtkGraphToPolyData()
			graphToPolyData.SetInputData(graph_vtk)
			graphToPolyData.Update()
			writer = vtkXMLPolyDataWriter()
			writer.SetInputData(graphToPolyData.GetOutput())

			writer.SetFileName('path_cluster_CS'+str(i_num)+'.vtp')

			writer.Write()
			del graph_vtk
			del points
			del weights
			del writer
			del graphToPolyData

np.save('advection_based_paths.npy', ordered)

print('path total time = ',(time.time()-start)/60)
