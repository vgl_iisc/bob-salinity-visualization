from scipy import ndimage
import itertools
import cc3d
import numpy as np
import netCDF4
import os
import vtk
from vtk import vtkPolyDataWriter, vtkMutableDirectedGraph, vtkPoints, vtkGraphToPolyData, vtkXMLPolyDataWriter
import time
import multiprocessing as mp
import datetime
import networkx as nx


# reading parameters from file
fin = open('parameter.txt', 'r')
Lines = fin.readlines()

factor = float(input('Input the factor : '))
low_vol = float(input('Input the lower limit : '))
up_vol = float(input('Input the upper limit : '))

smth = int(input('Smoothing filter size : '))
#fltr = float(input('Advcetion fraction : '))
min_clstr = int(input('Min cluster size : '))

parameters = []
for line in Lines:
	line = line.strip()
	if len(line)!=0 and line[0]!='#' and line[0]!=' ' and line[0]!='\n':
		parameters.append(line.split('=')[1].strip())
		#print(parameters)

infile = str(parameters[5])

filename = str(parameters[6])

v_limit = int(parameters[0])

lt_origin = int(parameters[1])
ln_origin = int(parameters[2])

lt_clip = int(parameters[3])
ln_clip = int(parameters[4])

interpolation = int(parameters[10])

d_max = int(parameters[7])

resolution = float(parameters[8].split('/')[0].strip())/float(parameters[8].split('/')[1].strip())

neighborhood = int(parameters[9])

v_name = str(parameters[11])

d_name = str(parameters[12])

nbrhd_a = int(input('neighbourhood = '))

nbrhd=int(nbrhd_a/2)

n_paths = int(parameters[15])

set_bound_ln=[]
set_bound_lt=[]

bnds = parameters[13].split(',')
for i in bnds:
	set_bound_ln.append(float(i.strip()))

bnds = parameters[14].split(',')
for i in bnds:
	set_bound_lt.append(float(i.strip()))

set_bound_lt = [lt_origin]+set_bound_lt
set_bound_ln = [ln_origin]+set_bound_ln

lt_sets = [t for t in range(len(set_bound_lt))]
ln_sets = [t for t in range(len(set_bound_ln))]

set_blt = [(lt-lt_origin)/resolution for lt in set_bound_lt]
set_bln = [(ln-ln_origin)/resolution for ln in set_bound_ln]

lt_clip = round((lt_clip-lt_origin)/resolution)
ln_clip = round((ln_clip-ln_origin)/resolution)

print(datetime.datetime.now())

def nxt_pt(pt, vel):

	(ln,lt,d) = pt
	(u,v,w) = vel

	ln1 = ln_origin+resolution*ln
	lt1 = lt_origin+resolution*lt

	ln1 = np.radians(ln1)
	lt1 = np.radians(lt1)

	r = 6371-(d/1000)

	c = u/r
	a = (np.sin(c/2))**2
	dln = 2*np.arcsin(np.sqrt(a)/np.cos(lt1))

	c = v/r
	a = (np.sin(c/2))**2
	dlt = 2*np.arcsin(np.sqrt(a))

	ln2 = dln+ln1
	lt2 = dlt+lt1

	ln2 = np.degrees(ln2)
	lt2 = np.degrees(lt2)

	ln2 = round((ln2-ln_origin)/resolution)
	lt2 = round((lt2-lt_origin)/resolution)
	d2 = round(d + w*1000)

	return((ln2,lt2,d2))

def connected_components(grid):
	sh=grid.shape
	i__, j__, k__ = np.nonzero(grid)
	lst = zip(i__, j__, k__)
	cluster_graph = nx.Graph()
	for (i,j,k) in lst:
		#print(i,j,k)
		i_l = np.arange(i-nbrhd,i+nbrhd+1)
		j_l = np.arange(j-nbrhd,j+nbrhd+1)
		k_l = np.arange(k-nbrhd,k+nbrhd+1)
		for nbr in itertools.product(i_l,j_l,k_l):
			if nbr==(i,j,k) or nbr[0]<0 or nbr[1]<0 or nbr[2]<0 or nbr[0]>=sh[0] or nbr[1]>=sh[1] or nbr[2]>=sh[2]:
				continue
			if grid[nbr[0],nbr[1],nbr[2]]>0:
			#	print(nbr)
				cluster_graph.add_edge((i,j,k), nbr)
		#input('bla')
	comps = nx.connected_components(cluster_graph)
	del grid

	u_id=0
	grid1 = np.zeros(sh)
	for comp in comps:
		u_id+=1
		for pt in comp:
			grid1[pt[0],pt[1],pt[2]] = u_id
	return grid1

def spherical_dist(lat1, lat2, lon1, lon2, depth): #distance in KM

	lon1 = ln_origin+resolution*lon1
	lon2 = ln_origin+resolution*lon2
	lat1 = lt_origin+resolution*lat1
	lat2 = lt_origin+resolution*lat2

	lon1 = np.radians(lon1)
	lon2 = np.radians(lon2)
	lat1 = np.radians(lat1)
	lat2 = np.radians(lat2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1
	a = np.sin(dlat / 2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2)**2

	c = 2 * np.arcsin(np.sqrt(a))

	r = 6371-(depth/1000)
	return(c * r)

def spherical_gradient(input_3d_mesh,axis=-1):
	axis_i = axis
	sh = input_3d_mesh.shape
	if axis_i==-1 or axis_i==1:
		dist = np.fromfunction(lambda dpth,lat,lon: spherical_dist(lat-1,lat,lon,lon,dpth),((sh[0], sh[1]+1, sh[2])))
		dist_prev = np.delete(dist,(-1),axis=1)
		dist_forw = np.delete(dist,(0),axis=1)

		A = np.pad(input_3d_mesh,((0,0),(0,1),(0,0)),'edge')
		B = np.pad(input_3d_mesh,((0,0),(1,0),(0,0)),'edge')
		diff = A-B
		diff_forw = np.delete(diff,(0),axis=1)
		diff_prev = np.delete(diff,(-1),axis=1)
		grad_1 = ((diff_forw/dist_forw)+(diff_prev/dist_prev))/2

	if axis_i==-1 or axis_i==2:
		dist = np.fromfunction(lambda dpth,lat,lon: spherical_dist(lat,lat,lon-1,lon,dpth),((sh[0], sh[1], sh[2]+1)))
		dist_prev = np.delete(dist,(-1),axis=2)
		dist_forw = np.delete(dist,(0),axis=2)

		A = np.pad(input_3d_mesh,((0,0),(0,0),(0,1)),'edge')
		B = np.pad(input_3d_mesh,((0,0),(0,0),(1,0)),'edge')
		diff = A-B
		diff_prev = np.delete(diff,(-1),axis=2)
		diff_forw = np.delete(diff,(0),axis=2)
		grad_2 = ((diff_forw/dist_forw)+(diff_prev/dist_prev))/2


	if axis_i==-1 or axis_i==0:
		dist_prev = 0.001 * np.ones(sh)
		dist_forw = 0.001 * np.ones(sh)

		A = np.pad(input_3d_mesh,((0,1),(0,0),(0,0)),'edge')
		B = np.pad(input_3d_mesh,((1,0),(0,0),(0,0)),'edge')
		diff = A-B
		diff_prev = np.delete(diff,(-1),axis=0)
		diff_forw = np.delete(diff,(0),axis=0)
		grad_0 = ((diff_forw/dist_forw)+(diff_prev/dist_prev))/2

	if axis==-1:
		grad = np.zeros((sh[0],sh[1],sh[2],3))
		grad[:,:,:,2] = np.copy(grad_2)
		grad[:,:,:,1] = np.copy(grad_1)
		grad[:,:,:,0] = np.copy(grad_0)
		return grad
	if axis==0:
		return grad_0
	if axis==1:
		return grad_1
	if axis==2:
		return grad_2

def euc_distance(p1,p2):
	dist = 0
	for i in range(len(p1)):
		diff = p1[i]-p2[i]
		dist += (diff**2)
	return np.sqrt(dist)

start = time.time()
'''
cmd = "rm data.nc"
os.system(cmd)
cmd = "cp "+infile+" data.nc"
os.system(cmd)
'''
DS = netCDF4.Dataset(infile, "r")

shape = DS.variables[v_name].shape

d_lst = np.copy(DS.variables[d_name][:])

print(d_max,' d_max')

print(shape)
'''
cores = os.cpu_count()
d_loop = []
t_loop = []
d,t=0,0

while(True):
	if d+cores<d_max:
		d_loop.append(np.arange(d,d+cores))
		d+=cores
	else:
		d_loop.append(np.arange(d,d_max))
		break
while(True):
	if t+cores<shape[0]:
		t_loop.append(np.arange(t,t+cores))
		t+=cores
	else:
		t_loop.append(np.arange(t,shape[0]))
		break
'''
fn = mp.RawArray('d',shape[0]*shape[1]*shape[2]*shape[3])
fn_v = mp.RawArray('d',shape[0]*shape[1]*shape[2]*shape[3])
fn_u = mp.RawArray('d',shape[0]*shape[1]*shape[2]*shape[3])

fn_np = np.frombuffer(fn, dtype=np.float64).reshape(shape)
fn_v_np = np.frombuffer(fn_v, dtype=np.float64).reshape(shape)
fn_u_np = np.frombuffer(fn_u, dtype=np.float64).reshape(shape)

v_gar = DS.variables['vo']._FillValue
u_gar = DS.variables['uo']._FillValue
s_gar = DS.variables['so']._FillValue

for t in range(shape[0]):
	fn_np[t] = np.copy(DS.variables[v_name][t].astype('float64'))
	fn_v_np[t] = np.copy(DS.variables["vo"][t].astype('float64'))
	fn_u_np[t] = np.copy(DS.variables["uo"][t].astype('float64'))

fn_np[fn_np==s_gar]=np.nan
fn_v_np[fn_v_np==v_gar]=np.nan
fn_u_np[fn_u_np==u_gar]=np.nan

fn_v_np = fn_v_np*(86.4) #Km/day conversion
fn_u_np = fn_u_np*(86.4) #Km/day conversion

def foo4(t):
	vel_v = np.zeros((d_max, shape[2], shape[3]))
	vel_u = np.zeros((d_max, shape[2], shape[3]))
	
	vel_u = np.copy(fn_u_np[t])
	vel_v = np.copy(fn_v_np[t])
	
	mask_u = np.copy(vel_u)
	mask_u[np.invert(np.isnan(mask_u))]=0
	
	mask_v = np.copy(vel_v)
	mask_v[np.invert(np.isnan(mask_v))]=0
	
	vel_u[np.isnan(vel_u)]=0
	vel_v[np.isnan(vel_v)]=0
	
	avg = ndimage.uniform_filter(vel_u, size=smth)
	vel_u = np.copy(avg)

	avg = ndimage.uniform_filter(vel_v, size=smth)
	vel_v = np.copy(avg)
	
	vel_u+=mask_u
	vel_v+=mask_v
	
	return vel_u,vel_v
	
print('vel data smoothing')
'''
for t in range(shape[0]):
	foo4(t)
'''
pool = mp.Pool()
UV=pool.map(foo4, np.arange(shape[0]))
pool.close()
pool.join()
del pool

for t in range(shape[0]):
	fn_v_np[t] = np.copy(UV[t][1]).astype('float64')
	fn_u_np[t] = np.copy(UV[t][0]).astype('float64')

del UV
'''
for _t in t_loop:
	pool = mp.Pool()
	pool.map(foo4, _t)
	pool.close()
	pool.join()
	del pool
'''
print('done vel data smoothing')

def foo5(t):
	vel_v = np.zeros((shape[1], shape[2], shape[3]))
	vel_u = np.zeros((shape[1], shape[2], shape[3]))
	vel_w = np.zeros((shape[1], shape[2], shape[3]))

	for d in range(1,shape[1]):
		vel_v[d] = np.copy( (fn_v_np[t,d]+fn_v_np[t,d-1])/2 )
		vel_u[d] = np.copy( (fn_u_np[t,d]+fn_u_np[t,d-1])/2 )
	
	for lt in range(1,shape[2]):
		vel_v[:,lt,:] = np.copy( (fn_v_np[t,:,lt,:]+fn_v_np[t,:,lt-1,:])/2 )
		vel_u[:,lt,:] = np.copy( (fn_u_np[t,:,lt,:]+fn_u_np[t,:,lt-1,:])/2 )
		
	for ln in range(1, shape[3]):
		vel_v[:,:,ln] = np.copy( (fn_v_np[t,:,:,ln]+fn_v_np[t,:,:,ln-1])/2 )
		vel_u[:,:,ln] = np.copy( (fn_u_np[t,:,:,ln]+fn_u_np[t,:,:,ln-1])/2 )
		
	#vel_u = np.copy(fn_u_np[t])
	#vel_v = np.copy(fn_v_np[t])
	
	grad_v = np.copy(spherical_gradient(vel_v, axis=1))
	grad_u = np.copy(spherical_gradient(vel_u, axis=2))
	
	#grad_v[np.isnan(grad_v)]=0
	#grad_u[np.isnan(grad_u)]=0
	grad_w = -1*(grad_u+grad_v)
	
	for d in range(1, shape[1]):
		vel_w[d] = grad_w[d]*((-d_lst[d]+d_lst[d-1])/1000)
		
	for d in range(1,shape[1]):
		vel_w[d] += vel_w[d-1]
	
	final = np.zeros((shape[1], shape[2], shape[3]))
	
	for lt in range(1,shape[2]-1):
		final[:,lt,:] = np.copy( (vel_w[:,lt,:]+vel_w[:,lt+1,:])/2 )
	
	for ln in range(1,shape[3]-1):
		final[:,:,ln] = np.copy( (vel_w[:,:,ln]+vel_w[:,:,ln+1])/2 )
	
	final[:,:,0] = np.copy( 2*vel_w[:,:,1]-final[:,:,1] )
	final[:,:,-1] = np.copy( 2*vel_w[:,:,-1]-final[:,:,-2] )
	
	final[:,0,:] = np.copy( 2*vel_w[:,1,:]-final[:,1,:] )
	final[:,-1,:] = np.copy( 2*vel_w[:,-1,:]-final[:,-2,:] )
		
	del vel_w
	vel_w = np.copy(final)
	del final
	
	'''for d in range(1,shape[1]):
		avg = np.copy(vel_w[d])
		avg[np.isnan(avg)]=0
		mask = np.copy(avg)
		mask[mask!=0]=1
		#avg = np.abs(avg)
		print('avg(m/day):',((np.sum(avg)/np.sum(mask))*1000),'   at', d)
		
	for d in range(1,shape[1]):
		avg = np.copy(vel_w[d])
		avg[np.isnan(avg)]=0
		print('min(m/day):',np.min(avg)*1000,'   max(m/day):',np.max(avg)*1000,'   at', d)
	input()'''
	
	vel_w = -1*vel_w
	return vel_w

print('vertical vel est')

'''for t in range(shape[0]):
	foo5(t)'''

pool = mp.Pool()
W = pool.map(foo5, np.arange(shape[0]))
pool.close()
pool.join()
del pool

'''
for _t in t_loop:
	pool = mp.Pool()
	pool.map(foo5, _t)
	pool.close()
	pool.join()
	del pool
'''

fn_w = mp.RawArray('d',shape[0]*shape[1]*shape[2]*shape[3])
fn_w_np = np.frombuffer(fn_w, dtype=np.float64).reshape(shape)
for t in range(shape[0]):
	fn_w_np[t] = np.copy(W[t]).astype('float64')

print('done vert_est')

del W

d_lst = np.round(d_lst)
np.save('d_lst.npy', d_lst)

def foo3(d):
	d_c = d
	while(d_c <= d_lst[-1]):
		di = np.where(d_lst == d_c)[0]
		if(len(di)==1):
			break
		d_c+=1
	di = di[0]

	for t in range(shape[0]):
		k_s = np.zeros((shape[2], shape[3]))
		k_v = np.zeros((shape[2], shape[3]))
		k_u = np.zeros((shape[2], shape[3]))
		k_w = np.zeros((shape[2], shape[3]))
		
		if d == d_lst[di]:
			k_s = np.copy(fn_np[t][di])
			k_v = np.copy(fn_v_np[t][di])
			k_u = np.copy(fn_u_np[t][di])
			k_w = np.copy(fn_w_np[t][di])
			
		else:
			v_mx = fn_np[t][di]
			v_mn = fn_np[t][di-1]
			alpha = (d_lst[di]-d) / (d_lst[di]-d_lst[di-1])
			val = ((alpha) * v_mn + (1-alpha) * v_mx)
			k_s = np.copy(val)

			v_mx = fn_v_np[t][di]
			v_mn = fn_v_np[t][di-1]
			alpha = (d_lst[di]-d) / (d_lst[di]-d_lst[di-1])
			val = ((alpha) * v_mn + (1-alpha) * v_mx)
			k_v = np.copy(val)

			v_mx = fn_u_np[t][di]
			v_mn = fn_u_np[t][di-1]
			alpha = (d_lst[di]-d) / (d_lst[di]-d_lst[di-1])
			val = ((alpha) * v_mn + (1-alpha) * v_mx)
			k_u = np.copy(val)
			
			v_mx = fn_w_np[t][di]
			v_mn = fn_w_np[t][di-1]
			alpha = (d_lst[di]-d) / (d_lst[di]-d_lst[di-1])
			val = ((alpha) * v_mn + (1-alpha) * v_mx)
			k_w = np.copy(val)

		k_s[:,0:ln_clip+1]=0
		k_s[0:lt_clip+1,:]=0
				
		k_v[:,0:ln_clip+1]=0
		k_v[0:lt_clip+1,:]=0

		k_u[:,0:ln_clip+1]=0
		k_u[0:lt_clip+1,:]=0

		k_w[:,0:ln_clip+1]=0
		k_w[0:lt_clip+1,:]=0

		np.save('vol/vol_s'+str(d)+'_'+str(t)+'.npy', k_s)
		np.save('vol/vol_u'+str(d)+'_'+str(t)+'.npy', k_u)
		np.save('vol/vol_v'+str(d)+'_'+str(t)+'.npy', k_v)
		np.save('vol/vol_w'+str(d)+'_'+str(t)+'.npy', k_w)

		k_s[np.isnan(k_s)]=0
		k_s[k_s<low_vol]=0
		k_s[k_s>up_vol]=0
		k_s[k_s>0]=1
		np.save('vol/isovol_s'+str(d)+'_'+str(t)+'.npy', k_s)

	del k_s
	del k_v
	del k_u

print('interpolating and iso_vol')

cmd = "rm -r vol"
os.system(cmd)
cmd = 'mkdir vol'
os.system(cmd)
'''
for d in range(d_max):
	foo3(d)

'''
pool = mp.Pool()
pool.map(foo3, np.arange(d_max))
pool.close()
pool.join()
del pool

'''
for _d in d_loop:
	pool = mp.Pool()
	pool.map(foo3, _d)
	pool.close()
	pool.join()
	del pool
'''
print('done interpolating and iso_vol')

del fn, fn_v, fn_u, fn_np, fn_v_np, fn_u_np, fn_w_np 

'''
def point_clock(lt,ln, ltr,lnr):
	if ltr>lt and lnr<=ln:
		lnr+=1
	elif lnr>ln and ltr>=lt:
		ltr-=1
	elif ltr<lt and lnr>=ln:
		lnr-=1
	elif lnr<ln and ltr<=lt:
		ltr+=1

	return(ltr,lnr)

def point_anti_clock(lt,ln, ltr,lnr):
	if ltr>=lt and lnr<ln:
		ltr-=1
	elif lnr<=ln and ltr<lt:
		lnr+=1
	elif ltr<=lt and lnr>ln:
		ltr+=1
	elif lnr>=ln and ltr>lt:
		lnr-=1
	return(ltr,lnr)
'''
def foo1(t):
	data = np.zeros((d_max, shape[2], shape[3]))
	vel = np.zeros((d_max, shape[2], shape[3], 3))
	for d in range(d_max):
		vol_s = np.load('vol/vol_s'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_v = np.load('vol/vol_v'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_u = np.load('vol/vol_u'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_w = np.load('vol/vol_w'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)

		data[d] = np.copy(vol_s)
		vel[d,:,:,2] = np.copy(vol_u)
		vel[d,:,:,1] = np.copy(vol_v)
		vel[d,:,:,0] = np.copy(vol_w)

	grad = np.copy(spherical_gradient(data))
	#grad = np.copy(np.transpose(grad, axes=[1,2,3,0]))

	#vel = np.copy(np.transpose(vel, axes=[1,2,3,0]))
	#(distanec between two points using lat and lon)
	adv = np.copy(np.sum(grad*vel, axis = 3))
	for d in range(d_max):
		np.save('adv/adv'+str(d)+'_'+str(t)+'.npy', adv[d])
		np.save('grad_space/grad_sal'+str(d)+'_'+str(t)+'.npy', grad[d])

print('advection')
	
cmd = "rm -r adv"
os.system(cmd)
cmd = 'mkdir adv'
os.system(cmd)

cmd = "rm -r grad_space"
os.system(cmd)
cmd = 'mkdir grad_space'
os.system(cmd)
'''
for t in range(shape[0]):
	foo1(t)
'''
pool = mp.Pool()
pool.map(foo1, np.arange(shape[0]))
pool.close()
pool.join()
del pool

'''
for _t in t_loop:
	pool = mp.Pool()
	pool.map(foo1, _t)
	pool.close()
	pool.join()
	del pool
'''
print('done advection')

def foo2(d):
	for t in range(shape[0]):
		k_s = np.load('vol/vol_s'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		adv = np.load('adv/adv'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)

		if t+1<shape[0]:
			k_sn = np.load('vol/vol_s'+str(d)+'_'+str(t+1)+'.npy', allow_pickle=True)
			total_mov = np.copy(k_s - k_sn)
		else:
			k_sn = np.load('vol/vol_s'+str(d)+'_'+str(t-1)+'.npy', allow_pickle=True)
			total_mov = np.copy(k_sn - k_s)

		'''
		max_mov = np.max(total_mov)
		(lt__, ln__) = np.where(total_mov<=(max_mov/fltr))
		lst = zip(lt__, ln__)

		for i in lst:
			total_mov[i]=0
			adv[i]=0
		'''

		dif = total_mov-adv

		total_mov = np.abs(adv)+np.abs(dif)
		
		ratio = np.abs(adv)/total_mov

		np.save('points/ratio'+str(d)+'_'+str(t)+'.npy', ratio)
		#np.save('points/nut'+str(d)+'_'+str(t)+'.npy', nut)

		del ratio
		del adv

print('points ratio')

cmd = "rm -r points"
os.system(cmd)
cmd = 'mkdir points'
os.system(cmd)
'''
for d in range(d_max):
	foo2(d)
'''
pool = mp.Pool()
pool.map(foo2, np.arange(d_max))
pool.close()
pool.join()
del pool

'''
for _d in d_loop:
	pool = mp.Pool()
	pool.map(foo2, _d)
	pool.close()
	pool.join()
	del pool
'''

print('points ratio done')

def foo7(t):
	for d in range(d_max):
		ratio = np.copy(np.load('points/ratio'+str(d)+'_'+str(t)+'.npy', allow_pickle=True))
		ratio[np.isnan(ratio)]=0
		ratio[ratio<factor]=0
		ratio[ratio>0]=1
		iso_vol = np.copy(np.load('vol/isovol_s'+str(d)+'_'+str(t)+'.npy', allow_pickle=True))

		adv_pts = ratio*iso_vol
		np.save('points/adv_pts'+str(d)+'_'+str(t)+'.npy', adv_pts)

print('points adv_pts')
'''
for t in range(shape[0]):
	foo7(t)
'''
pool = mp.Pool()
pool.map(foo7, np.arange(shape[0]))
pool.close()
pool.join()
del pool


'''

for _t in t_loop:
	pool = mp.Pool()
	pool.map(foo7, _t)
	pool.close()
	pool.join()
	del pool
'''

print('points adv_pts done')

def foo6(t): #computing labels
	cluster_rep = dict()
	adv_pts = np.zeros((d_max, shape[2], shape[3]))
	for d in range(d_max):
		adv_pts[d] = np.copy(np.load('points/adv_pts'+str(d)+'_'+str(t)+'.npy', allow_pickle=True))

	labels = connected_components(adv_pts)

	del adv_pts
	adv_frnt = np.copy(labels)

	#a = np.where(adv_frnt==1)
	#print(a)
	'''
	(d__,lt__,ln__)=np.nonzero(labels)
	lst = zip(d__,lt__,ln__)
	del lt__,ln__,d__
	
	for (d,lt,ln) in lst:
		label = labels[d,lt,ln]
		d__ = np.arange(d-1,d+1+1)
		lt__ = np.arange(lt-1,lt+1+1)
		ln__ = np.arange(ln-1,ln+1+1)

		for (d1,lt1,ln1) in itertools.product(d__,lt__,ln__):
			if d1<0 or d1>d_max-1 or lt1<0 or lt1>shape[2]-1 or ln1<0 or ln1>shape[3]-1:
				continue
			labels[d1,lt1,ln1]=label'''
	
	num_labels = np.max(adv_frnt)
	#print(num_labels)

	for d in range(d_max):
		np.save('points/adv_frnt'+str(d)+'_'+str(t)+'.npy', adv_frnt[d])
		np.save('points/labels'+str(d)+'_'+str(t)+'.npy', labels[d])


	(d__,lt__,ln__)=np.nonzero(adv_frnt)
	for itr in range(len(d__)):
		(d,lt,ln) = (d__[itr],lt__[itr],ln__[itr])
		label = labels[d,lt,ln]
		if cluster_rep.get(label):
			cluster_rep[label][1]+=d
			cluster_rep[label][2]+=lt
			cluster_rep[label][3]+=ln
			cluster_rep[label][4]+=1
		else:
			cluster_rep[label]=[t,d,lt,ln,1]

	cluster_rep_new=dict()
	to_del = []

	for label, [t1,d1,lt1,ln1,ct1] in cluster_rep.items():
		if ct1<min_clstr:
			to_del.append(label)
		else:
			cluster_rep[label] = [t1,d1/ct1,lt1/ct1,ln1/ct1,ct1,(d_max+1)*(shape[2]+1)*(shape[3]+1)]
			cluster_rep_new[label]= [t1,d1/ct1,lt1/ct1,ln1/ct1,ct1]

	[cluster_rep. pop(label) for label in to_del]
	del to_del

	for itr in range(len(d__)):
		(d,lt,ln) = (d__[itr],lt__[itr],ln__[itr])
		label = labels[d,lt,ln]
		if cluster_rep.get(label):
			(t1,d1,lt1,ln1,ct1,dist1) = cluster_rep[label]
		else:
			continue
		dist = euc_distance((d,lt,ln),(d1,lt1,ln1))
		if dist<dist1:
			cluster_rep_new[label] = [t,d,lt,ln,ct1]
			cluster_rep[label][5]=dist

	np.save('vol/cluster_rep'+str(t)+'.npy', cluster_rep_new)
	return num_labels+1

print('adv_front')

max_label = 0
'''
for t in range(shape[0]):
	foo6(t)
'''
pool = mp.Pool()
n_labels = np.copy(pool.map(foo6, np.arange(shape[0])))
max_label=np.max(n_labels)
pool.close()
pool.join()
del pool
'''

for _t in t_loop:
	pool = mp.Pool()
	n_labels = np.copy(pool.map(foo6, _t))
	if max_label<np.max(n_labels): max_label=np.max(n_labels)
	pool.close()
	pool.join()
	del pool
'''

np.save('vol/max_label.npy', max_label)

print('adv_front done')

#max_label = 10000
def foo8(t):
	if t>=shape[0]-1: return
	for d in range(d_max):
		edges = set()
		adv_pts = np.load('points/adv_frnt'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		labels = np.load('points/labels'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_v = np.load('vol/vol_v'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_u = np.load('vol/vol_u'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)
		vol_w = np.load('vol/vol_w'+str(d)+'_'+str(t)+'.npy', allow_pickle=True)

		(lt__,ln__)=np.nonzero(adv_pts)
		for itr in range(len(lt__)):
			(lt,ln) = (lt__[itr],ln__[itr])
			p1 = (ln,lt,d)
			p2 = nxt_pt(p1, (vol_u[lt,ln], vol_v[lt,ln], vol_w[lt,ln]))
			if p2[0]<0 or p2[0]>=shape[3] or p2[1]<0 or p2[1]>=shape[2] or p2[2]<0 or p2[2]>=d_max:
				continue
			'''adv_pts_n = np.load('points/labels'+str(p2[2])+'_'+str(t+1)+'.npy', allow_pickle=True)
			e = ((t,adv_pts[p1[1],p1[0]]),(t+1,adv_pts_n[p2[1],p2[0]]))
			if adv_pts_n[p2[1],p2[0]]!=0:
				edges.add(e)'''
			if p1[2]==p2[2]:
				adv_pts_n = np.load('points/labels'+str(p2[2])+'_'+str(t+1)+'.npy', allow_pickle=True)
				e = ((t,adv_pts[p1[1],p1[0]]),(t+1,adv_pts_n[p2[1],p2[0]]))
				if adv_pts_n[p2[1],p2[0]]!=0:
					edges.add(e)
				continue

			ds = []
			if vol_w[lt,ln]>0:
				ds = np.arange(p1[2]+1,p2[2]+1)
				#print(d,round(d+1+vol_w[lt,ln]))
			if vol_w[lt,ln]<0:
				ds = np.arange(p2[2],p1[2])
				#print(round(d+vol_w[lt,ln]),d+1)

			all_intersections=[]
			all_labels=dict()
			
			for dt in ds:
				if dt<0 or dt>=d_max:
					continue
				all_labels[dt] = np.load('points/labels'+str(dt)+'_'+str(t+1)+'.npy', allow_pickle=True)

				lnt = round((((dt-p1[2])/(p2[2]-p1[2]))*(p2[0]-p1[0]))+p1[0])
				ltt = round((((dt-p1[2])/(p2[2]-p1[2]))*(p2[1]-p1[1]))+p1[1])

				all_intersections.append((lnt,ltt,dt))
			for (ln1,lt1,d1) in all_intersections:
				if lt1<0 or ln1<0 or lt1>=shape[2] or ln1>=shape[3] or d1<0 or d1>=d_max:
					continue
				e = ((t,adv_pts[lt,ln]),(t+1,all_labels[d1][lt1,ln1]))
				if all_labels[d1][lt1,ln1]!=0:
					edges.add(e)
			del all_labels, all_intersections
		edges = np.copy(list(edges))
		np.save('edges/edges'+str(d)+'_'+str(t)+'.npy', edges)
	#print('done',t)

print('edges')

cmd = "rm -r edges"
os.system(cmd)
cmd = 'mkdir edges'
os.system(cmd)
'''
for t in range(shape[0]):
	foo8(t)
'''
pool = mp.Pool()
pool.map(foo8, np.arange(shape[0]))
pool.close()
pool.join()
del pool
'''

for _t in t_loop:
	pool = mp.Pool()
	pool.map(foo8, _t)
	pool.close()
	pool.join()
	del pool
'''
print('edges done')

graph_vtk1 = vtkMutableDirectedGraph()

points1 = vtkPoints()

weights1 = vtk.vtkDoubleArray()
weights1.SetNumberOfComponents(1)
weights1.SetName('Cluster Size')

weights2 = vtk.vtkDoubleArray()
weights2.SetNumberOfComponents(1)
weights2.SetName('Cluster Number')

weights3 = vtk.vtkDoubleArray()
weights3.SetNumberOfComponents(1)
weights3.SetName('Time')

for t in range(shape[0]-1):
	cluster_rep = np.load('vol/cluster_rep'+str(t)+'.npy', allow_pickle=True).item()
	cluster_rep1 = np.load('vol/cluster_rep'+str(t+1)+'.npy', allow_pickle=True).item()

	for d in range(d_max):
		edges = np.copy(np.load('edges/edges'+str(d)+'_'+str(t)+'.npy', allow_pickle=True))
		
		for (p1,p2) in edges:
			(t1,cn1) = p1
			(t2,cn2) = p2
			if cluster_rep.get(cn1): (_,d1,lt1,ln1,cs1) = cluster_rep[cn1]
			else: continue
			if cluster_rep1.get(cn2): (_,d2,lt2,ln2,cs2) = cluster_rep1[cn2]
			else: continue
			x1 = ln_origin+(resolution*int(ln1))
			y1 = lt_origin+(resolution*int(lt1))

			x2 = ln_origin+(resolution*int(ln2))
			y2 = lt_origin+(resolution*int(lt2))

			vertex1 = graph_vtk1.AddVertex()
			points1.InsertNextPoint([x1, y1, -d1])
			weights1.InsertNextValue(cs1)
			weights2.InsertNextValue(t1*max_label+cn1)
			weights3.InsertNextValue(t1)
			vertex2 = graph_vtk1.AddVertex()
			points1.InsertNextPoint([x2, y2, -d2])
			weights1.InsertNextValue(cs2)
			weights2.InsertNextValue(t2*max_label+cn2)
			weights3.InsertNextValue(t2)
			graph_vtk1.AddEdge (vertex1, vertex2)

graph_vtk1.SetPoints(points1)
graph_vtk1.GetVertexData().AddArray(weights1);
graph_vtk1.GetVertexData().AddArray(weights2);
graph_vtk1.GetVertexData().AddArray(weights3);

graphToPolyData = vtkGraphToPolyData()
graphToPolyData.SetInputData(graph_vtk1)
graphToPolyData.Update()
writer = vtkXMLPolyDataWriter()
writer.SetInputData(graphToPolyData.GetOutput())
name = str(factor)+'-'+str(low_vol)+'-'+str(up_vol)+'-'+str(nbrhd_a)
writer.SetFileName('NEW_TG_'+name+'.vtp')
writer.Write()


print('done\n********************\ntotal time = ',(time.time()-start)/60)
#for cluster in range(len(final_clusters)): #insert edges
